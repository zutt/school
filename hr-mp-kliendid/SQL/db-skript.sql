/* 
 * SEQUENCE nr kustutamine ...
 */

DROP SEQUENCE customer_id CASCADE;
DROP SEQUENCE s_cst_address CASCADE;
DROP SEQUENCE s_employee CASCADE;
DROP SEQUENCE s_c_group CASCADE;
DROP SEQUENCE s_customer_group CASCADE;
DROP SEQUENCE s_emp_user CASCADE;
DROP SEQUENCE s_comm_device_type CASCADE;
DROP SEQUENCE s_comm_device CASCADE;

/* 
 * INDEKSite kustutamine 
 */

DROP INDEX customer_idx1; 
DROP INDEX customer_idx2; 
DROP INDEX customer_idx3; 
DROP INDEX customer_idx5; 
DROP INDEX customer_idx6;
DROP INDEX customer_idx7;
DROP INDEX customer_idx8;
DROP INDEX customer_idx9;
DROP INDEX customer_idx10;
DROP INDEX customer_idx11;

DROP INDEX customer_group_idx1;
DROP INDEX customer_group_idx2;
DROP INDEX customer_group_idx3;

DROP INDEX c_group_idx1;

DROP INDEX comm_device_idx1;
DROP INDEX comm_device_idx2;
DROP INDEX comm_device_idx3;
DROP INDEX comm_device_idx4;

DROP INDEX cst_address_idx1;
DROP INDEX cst_address_idx2;
DROP INDEX cst_address_idx3;
DROP INDEX cst_address_idx4;
DROP INDEX cst_address_idx5;
DROP INDEX cst_address_idx6;
DROP INDEX cst_address_idx7;

DROP INDEX employee_idx1;
DROP INDEX employee_idx2;
DROP INDEX employee_idx3;
DROP INDEX employee_idx4;

DROP INDEX emp_user_ux1;
DROP INDEX emp_user_idx1;
DROP INDEX emp_user_idx2;
DROP INDEX emp_user_idx3;

/* 
 * TABELITE kustutamine 
 */

DROP TABLE customer CASCADE;
DROP TABLE cst_address CASCADE;
DROP TABLE employee CASCADE;
DROP TABLE c_group CASCADE;
DROP TABLE customer_group CASCADE;
DROP TABLE emp_user CASCADE;
DROP TABLE comm_device_type CASCADE;
DROP TABLE comm_device CASCADE;

/* 
 * TABELITE LOOMISE LAUSED 
 */

CREATE SEQUENCE  customer_id  START 1;

CREATE TABLE customer (
       customer            NUMERIC(30,0)  CONSTRAINT customer_pk PRIMARY KEY DEFAULT NEXTVAL('customer_id'),
       first_name          VARCHAR(100),
       last_name           VARCHAR(100),
       identity_code       VARCHAR(20),
       note                VARCHAR(1000),
       created             TIMESTAMP,
       updated             TIMESTAMP,
       created_by          NUMERIC(30,0),
       updated_by          NUMERIC(30,0),
       birth_date          TIMESTAMP,
       cst_type            NUMERIC(2,0),
       cst_state_type      NUMERIC(2,0)   
);

CREATE SEQUENCE  s_cst_address  START 1;

CREATE TABLE cst_address (
    cst_address                  NUMERIC(30,0) CONSTRAINT cst_address_pk PRIMARY KEY DEFAULT NEXTVAL('s_cst_address'),
    customer                       NUMERIC(30,0),
    zip                            VARCHAR(20),
    house                          VARCHAR(100),
    address                        VARCHAR(100),
    county                         VARCHAR(100),
    parish                         VARCHAR(100),
    town_county                    VARCHAR(100),
    address_type                   NUMERIC(30,0),
    phone                          VARCHAR(20),
    sms                            VARCHAR(20),
    mobile                         VARCHAR(20),
    email                          VARCHAR(30),
    note                           VARCHAR(50),
    country                        NUMERIC(30,0),
    created                        TIMESTAMP,
    updated                        TIMESTAMP,
    created_by                     NUMERIC(30,0),
    updated_by                     NUMERIC(30,0)
 );



CREATE SEQUENCE  s_employee  START 1;

CREATE TABLE employee (
    employee                       NUMERIC(30,0) CONSTRAINT employee_pk PRIMARY KEY DEFAULT NEXTVAL('s_employee'),
    first_name                     VARCHAR(50),
    last_name                      VARCHAR(50),
    emp_code                       VARCHAR(20),
    created_by                     NUMERIC(30,0),
    updated_by                     NUMERIC(30,0),
    current_position               NUMERIC(30,0),
    current_manager                NUMERIC(30,0),
    current_struct_unit            NUMERIC(30,0),
    created                        TIMESTAMP,
    updated                        TIMESTAMP,
    emp_role                       NUMERIC(30,0)
 );

CREATE SEQUENCE  s_c_group  START 1;

CREATE TABLE c_group (
    c_group                     NUMERIC(30,0) CONSTRAINT c_group_pk PRIMARY KEY DEFAULT NEXTVAL('s_c_group'),
    struct_unit                 NUMERIC(30,0),
    name                        VARCHAR(100),
    description                 VARCHAR(300),
    created                     TIMESTAMP,
    created_by                  NUMERIC(30,0),
    updated                     TIMESTAMP,
    updated_by                  NUMERIC(30,0)
);


CREATE SEQUENCE  s_customer_group  START 1;

CREATE TABLE customer_group (
    customer_group                    NUMERIC(30,0) CONSTRAINT customer_group_pk PRIMARY KEY DEFAULT NEXTVAL('s_customer_group'),
    customer                          NUMERIC(30,0),
    c_group                           NUMERIC(30,0),
    created                           TIMESTAMP,
    created_by                        NUMERIC(30,0)
);



CREATE SEQUENCE  s_emp_user  START 1;

CREATE TABLE emp_user (
    emp_user                          NUMERIC(30,0) CONSTRAINT emp_user_pk PRIMARY KEY DEFAULT NEXTVAL('s_emp_user'),
    employee                          NUMERIC(30,0),
    username                          VARCHAR(20),
    passw                             VARCHAR(300),
    user_status_type                  NUMERIC(30,0),
    created                           TIMESTAMP,
    created_by                        NUMERIC(30,0),
    updated                           TIMESTAMP,
    updated_by                        NUMERIC(30,0),
    last_session                      NUMERIC(30,0),
    contact_email                     VARCHAR(20)
);


CREATE SEQUENCE  s_comm_device_type  START 1;

CREATE TABLE comm_device_type (
    comm_device_type              NUMERIC(30,0) CONSTRAINT comm_device_type_pk PRIMARY KEY DEFAULT NEXTVAL('s_comm_device_type'),
    name                          VARCHAR(100) NULL,
    description                   VARCHAR(300) NULL
);

CREATE SEQUENCE  s_comm_device   START 1;

CREATE TABLE comm_device  (
    comm_device                   NUMERIC(30,0) CONSTRAINT comm_device_pk PRIMARY KEY DEFAULT NEXTVAL('s_comm_device'),
    comm_device_type              NUMERIC(30,0) NULL,
    customer                      NUMERIC(30,0) NULL,
    value_text                    VARCHAR(100) NULL,
    orderb                        NUMERIC(30,0) NULL,
    created                       TIMESTAMP
);

/*
 * INDEKSID JA PIIRANGUD
 */	

/* CUSTOMER tabeli indeksid ja piirangud */

CREATE  INDEX customer_idx1
 ON customer
  ( customer ) ;

CREATE  INDEX customer_idx2
 ON customer
  ( last_name varchar_pattern_ops ) ;

CREATE  INDEX customer_idx3
 ON customer
  ( identity_code varchar_pattern_ops ) ;

CREATE  INDEX customer_idx5
 ON customer
  ( birth_date ) ;
  
  CREATE  INDEX customer_idx6
 ON customer
  ( first_name varchar_pattern_ops) ;

CREATE  INDEX customer_idx7
 ON customer
  ( created ) ;

CREATE  INDEX customer_idx8
 ON customer
  ( created_by ) ;

CREATE  INDEX customer_idx9
 ON customer
  ( updated ) ;

CREATE  INDEX customer_idx10
 ON customer
  ( updated_by ) ;

CREATE  INDEX customer_idx11
 ON customer
  ( note varchar_pattern_ops) ;



/* CUSTOMER_GROUP tabeli indeksid ja piirangud */

CREATE  INDEX customer_group_idx1
 ON customer_group
  ( customer ) ;

CREATE  INDEX customer_group_idx2
 ON customer_group
  ( c_group ) ;

CREATE  INDEX customer_group_idx3
 ON customer_group
  ( customer_group ) ;


/* C_GROUP tabeli indeksid ja piirangud */

CREATE  INDEX c_group_idx1
 ON c_group
  ( c_group) ;


/* COMM_DEVICE_TYPE tabeli indeksid ja piirangud */



/* COMM_DEVICE tabeli indeksid ja piirangud */


CREATE  INDEX comm_device_idx1
 ON comm_device
  ( customer) ;

CREATE  INDEX comm_device_idx2
 ON comm_device
  ( comm_device_type) ;

CREATE  INDEX comm_device_idx3
 ON comm_device
  ( value_text varchar_pattern_ops) ;

CREATE  INDEX comm_device_idx4
 ON comm_device
  ( comm_device) ;


/* CST_ADDRESS tabeli indeksid ja piirangud */


CREATE  INDEX cst_address_idx1
 ON cst_address
  ( customer  ) ;

CREATE  INDEX cst_address_idx2
 ON cst_address
  ( address varchar_pattern_ops ) ;

CREATE  INDEX cst_address_idx3
 ON cst_address
  ( county varchar_pattern_ops) ;

CREATE  INDEX cst_address_idx4
 ON cst_address
  ( house varchar_pattern_ops) ;

CREATE  INDEX cst_address_idx5
 ON cst_address
  ( town_county varchar_pattern_ops) ;

CREATE  INDEX cst_address_idx6
 ON cst_address
  ( zip varchar_pattern_ops) ;

CREATE  INDEX cst_address_idx7
 ON cst_address
  ( cst_address) ;


/* EMPLOYEE tabeli indeksid ja piirangud */

CREATE  INDEX employee_idx1
 ON employee
  (first_name varchar_pattern_ops) ;

CREATE  INDEX employee_idx2
 ON employee
  (last_name varchar_pattern_ops) ;

CREATE  INDEX employee_idx3
 ON employee
  (employee) ;

CREATE  INDEX employee_idx4
 ON employee
  (emp_code) ;


/* EMP_USER tabeli indeksid ja piirangud */


CREATE UNIQUE INDEX emp_user_ux1 
	ON emp_user
  (username   varchar_pattern_ops);

CREATE  INDEX emp_user_idx1
 ON emp_user
  (employee) ;

CREATE  INDEX emp_user_idx2
 ON emp_user
  (passw varchar_pattern_ops) ;

CREATE  INDEX emp_user_idx3
 ON emp_user
  (emp_user) ;

 /* TABELITESSE ANDMETE LISAMISE LAUSED */


/* 'customer' tabelisse andmete lisamine*/

INSERT INTO customer (first_name, last_name, identity_code, note, birth_date,created,created_by, updated, updated_by) VALUES 
					('Jaan','Tamm','55555','markus1','1970-10-01','2014-01-20',1,now(),2),
					('Jaana','Tamm','77777','markus2','1987-12-27','2014-02-21',2,now(),2),
					('Janek','Tamm','44444','markus3','1967-04-28','2014-03-22',3,now(),2);


/* 'cst_address' tabelisse andmete lisamine*/
 
INSERT INTO cst_address (customer,zip,house,address,county,town_county) VALUES 
					(1,'93810','3','Aia','Saaremaa','Kuressaare'),
					(2,'88301','5','Metsa','Parnumaa','Audru'),
					(3,'11621','8','Kuuse','Harjumaa','Tallinn');
					

 /* 'employee' tabelisse andmete lisamine */
 
 INSERT INTO employee (first_name,last_name,emp_code) VALUES 
					('Aita','Metsa','11111'),
					('Jaani','Ussike','22222'),
					('Triinu','Lepa','33333');


 /* 'c_group' tabelisse andmete lisamine */
 
 INSERT INTO c_group (name) VALUES 
					('Single'),
					('Married'),
					('Divorced'),
					('Happy'),
					('Unhappy');


 /* 'customer_group' tabelisse andmete lisamine */

INSERT INTO customer_group (c_group,customer,created_by) VALUES 
					(1,1,1),
					(2,2,3),
					(1,3,2);
					
/* 'emp_user' tabelisse andmete lisamine */

INSERT INTO emp_user (employee,username,passw) VALUES 
					(1,'aita.metsa','1234'),
					(2,'jaani.ussike','2234'),
					(3,'triinu.lepa','3234');

/* 'comm_device_type' tabelisse andmete lisamine */

INSERT INTO comm_device_type (name) VALUES 
					('phone'),
					('e-mail'),
					('fax');					

/* 'comm_device' tabelisse andmete lisamine */

INSERT INTO comm_device (customer,comm_device_type,value_text,orderb,created) VALUES 
					(1,1,'+372 55 445 88','1',now()),
					(1,2,'jhjkhjkjh','2',now()),
					(2,1,'+372 37 445 99','1',now()),
					(3,1,'+372 56 4675 89','1',now());
/* ----- */



