/**
 * @author martinparoll
 *
 */

var ajaxserver_url="/hr-mp-kliendid/ajax";
	
	//helping functions so we can write shorter JS
	function gid(x) { return document.getElementById(x); } 
	function log(x) {  console.log(x); }
	
	// hide any div
	function hideme(divid){
		//console.log("hide" + divid);
		gid(divid).style.visibility="hidden";
	}
	
	// show any div
	function showme(divid)
	{
		// log('SHow' + divid);
		gid(divid).style.visibility="visible";
	}
	
	// hide notes for main view
	function hide_customer_note(customer_code) 
	{ 
		//log('clicked hide');
	 	var divid = "customer_note_" + customer_code;
	 	var noteid = "note_" + customer_code;
	 	//log(noteid);
	 	element = gid(divid); 
	 	var link_text="<A HREF='javascript:show_customer_note(" + customer_code + ")'>ShowNote</a>"; 
	 	gid(noteid).style.visibility="hidden";
	 	element.innerHTML = link_text ; 
	}
	
	// show notes for main view
	function show_customer_note(customer_code) 
	{ 
		//log('clicked show');
		var divid = "customer_note_" + customer_code; 
		var noteid = "note_" + customer_code;
		//log(noteid);
		element = gid(divid); 
		var link_text="<A HREF='javascript:hide_customer_note(" + customer_code + ")'>HideNote</a>";
		gid(noteid).style.visibility="visible";
		element.innerHTML = link_text ; 
	}	

	//ajax testing
	var request; 
	
	function sendIdPost(id){
		//var roll = gid("roll").value;
		var url = ajaxserver_url + "?action=postid" + "&customer_id="+id;
		if(window.ActiveXObject){ request = new ActiveXObject("Microsoft.XMLHTTP"); }
		else if(window.XMLHttpRequest){ request = new XMLHttpRequest(); } request.onreadystatechange = showResult;
		request.open("POST",url,true);
		request.send();
	}
	
	function sendUpdatePost(url){
		var updte_timestamp = new Date();
		var tm=updte_timestamp.getTime();

		var customer_id = gid("customer_id").value;
		var first_name = gid("first_name").value;
		var last_name = gid("last_name").value;
		var identity_code = gid("identity_code").value;
		
		var url= ajaxserver_url + "?action=update&customer_id="+customer_id+"&first_name="+ first_name + "&last_name="+ last_name + "&identity_code=" + identity_code + "&tm="+tm ;
		// alert(url);
		//url = encodeURI(url);

		if(window.ActiveXObject){ request = new ActiveXObject("Microsoft.XMLHTTP"); }
		else if(window.XMLHttpRequest){ request = new XMLHttpRequest(); } request.onreadystatechange = alert("sendUpdatePost vastus käes!?");
		request.open("POST",url,true);
		request.send();
	}
	
	
	function showResult(){
		if(request.readyState == 4){
			var response = request.responseXML;
			var responseArray = response.getElementsByTagName("Customer");
			var responseItem = responseArray[0];
			//log('Login showResulti');
			gid("NamelH1").innerHTML = responseItem.getElementsByTagName("First")[0].childNodes[0].data;
			gid("HostelH1").innerHTML = responseItem.getElementsByTagName("Last")[0].childNodes[0].data;
			gid("ContactH1").innerHTML = responseItem.getElementsByTagName("IDcode")[0].childNodes[0].data;
			//gid("ContactH2").innerHTML = responseItem.getElementsByTagName("IDcode")[0].childNodes[0].data;
			
			gid("customer_id").value = responseItem.getElementsByTagName("CustomerID")[0].childNodes[0].data;
			gid("first_name").value = responseItem.getElementsByTagName("First")[0].childNodes[0].data;
			gid("last_name").value = responseItem.getElementsByTagName("Last")[0].childNodes[0].data;
			gid("identity_code").value = responseItem.getElementsByTagName("IDcode")[0].childNodes[0].data;
			
		}
	}
	
	function show_customer_nr(customer_code) {
		sendIdPost(customer_code);
		showme('bottomform');
			
	}
		
	function save_customer()
	{
		sendUpdatePost();

	}
	
	
	function Initialize_dc()
	{
		try
		{
			req=new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch(e)
		{
			try
			{
				req=new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(oc)
			{
				req=null;
			}
		}

		if(!req&&typeof XMLHttpRequest!="undefined")
		{
			req= new XMLHttpRequest();
			mozillus = 1;
		}
	} 