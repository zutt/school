/**
 * Õppejõu näite JS 
 * 
 */


var req;
var my_divid;
var mozillus = 0;
var appserver_url = "http://imbi.ld.ttu.ee:7500/ajax/";


function rendid()
{
	var auto_id = document.forms['car_form'].id.value;

	Initialize_dc(); 

	var url=appserver_url + "RentXMLService?auto="+auto_id;
	url = encodeURI(url);

	if(req!=null)
	{
		req.onreadystatechange = Process_rent_request;
		req.open("GET", url, true);
		req.send(null);

	}




}

function select_car(auto_id)
{
	show_car_form();
	get_car_xml(auto_id);
	clear_rents();
}



function show_car_form(auto_id)
{

	ShowDiv("div_car_form");
	document.getElementById("car_form_response").innerHTML="";
}

function hide_car_form()
{

	HideDiv("div_car_form");

}


function ShowDiv(divid)
{
	if (document.layers) document.layers[divid].visibility="show";
	else document.getElementById(divid).style.visibility="visible";
}

function HideDiv(divid)
{
	if (document.layers) document.layers[divid].visibility="hide";
	else document.getElementById(divid).style.visibility="hidden";
}



function show_car_note(auto_kood)
{

	var divid = "car_note_" + auto_kood;
	var divid_input = "car_note_input_" + auto_kood;
	element = document.getElementById(divid);
	var link_text="<A HREF='javascript:hide_car_note(" + auto_kood + ")'>peida</a>";
//	element.innerHTML = document.forms['auto_read'].elements[divid_input].value + link_text ;
	element.innerHTML = document.forms['auto_read'].elements[divid_input].value + link_text ;
}

function hide_car_note(auto_kood)
{
	var divid = "car_note_" + auto_kood;
	element = document.getElementById(divid);
	var link_text="<A HREF='javascript:show_car_note(" + auto_kood + ")'>peida</a>";
	element.innerHTML = link_text ;

}

function clear_rents()
{

	var t = document.getElementById("mutabel");

	while(t.rows.length )
	{
		t.deleteRow(0);
	}

}

function parseXMLResult(xml)
{
	var xml_doc ;
	if (mozillus == 0)
	{

		xml_doc = xml.documentElement;
	}
	else
	{
		xml_doc = xml.documentElement;
	}

	var xml_data_value ;

	xml_data_value = xml_doc.getElementsByTagName('CAR_ID')[0].firstChild.nodeValue;

	document.forms['car_form'].id.value = xml_data_value;
	xml_data_value = xml_doc.getElementsByTagName("MARK")[0].childNodes[0].nodeValue;
	document.forms['car_form'].mark.value = xml_data_value;

	xml_data_value = xml_doc.getElementsByTagName("HIND")[0].childNodes[0].nodeValue;
	document.forms['car_form'].hind.value = xml_data_value;

	xml_data_value = xml_doc.getElementsByTagName("KIRJELDUS")[0].childNodes[0].nodeValue;
	document.forms['car_form'].kirjeldus.value = xml_data_value;

	xml_data_value = xml_doc.getElementsByTagName("VALJALASE")[0].childNodes[0].nodeValue;
	document.forms['car_form'].valjalase.value = xml_data_value;


}


function parseRentXMLResult(xml)
{
	var rent ;
	var xml_doc = xml.documentElement;
	var rents = xml_doc.getElementsByTagName('RENT');
	var len = rents.length ;
	var tbl ;
	var lastRow ;
	var row ;
	var cell ;
	var is_rows = 0;

	for (var i=0; i < len; i++) {
		rent = rents[i];
		is_rows = 1;
		// alert(rent.getElementsByTagName("RENT_ID")[0].childNodes[0].nodeValue);
		tbl = document.getElementById("mutabel");
		lastRow = tbl.rows.length;
		row = tbl.insertRow(lastRow);



		cell = row.insertCell(0);
		cell.innerHTML = rent.getElementsByTagName("STAATUS")[0].childNodes[0].nodeValue ;


		cell = row.insertCell(0);
		cell.innerHTML = rent.getElementsByTagName("MAKSTUD")[0].childNodes[0].nodeValue ;


		cell = row.insertCell(0);
		cell.innerHTML = rent.getElementsByTagName("MAKSUMUS")[0].childNodes[0].nodeValue ;


		cell = row.insertCell(0);
		cell.innerHTML = rent.getElementsByTagName("LOPP")[0].childNodes[0].nodeValue ;


		cell = row.insertCell(0);
		cell.innerHTML = rent.getElementsByTagName("ALGUS")[0].childNodes[0].nodeValue ;


		cell = row.insertCell(0);
		cell.innerHTML = rent.getElementsByTagName("K_PERENIMI")[0].childNodes[0].nodeValue ;


		cell = row.insertCell(0);
		cell.innerHTML = rent.getElementsByTagName("K_EESNIMI")[0].childNodes[0].nodeValue ;

		cell = row.insertCell(0);
		cell.innerHTML = rent.getElementsByTagName("RENT_ID")[0].childNodes[0].nodeValue ;



	}
	if (is_rows > 0)
	{
		lastRow = tbl.rows.length;
		row = tbl.insertRow(lastRow);
		cell = row.insertCell(0);
		cell.innerHTML = "<a href='javascript:clear_rents()'>KINNI</a>";
	}


}






function Initialize_dc()
{
	try
	{
		req=new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch(e)
	{
		try
		{
			req=new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(oc)
		{
			req=null;
		}
	}

	if(!req&&typeof XMLHttpRequest!="undefined")
	{
		req= new XMLHttpRequest();
		mozillus = 1;

	}


} 





function get_car_xml(auto_id)
{


	Initialize_dc(); 

	var url=appserver_url + "FrontController?mode=XML&auto="+auto_id;
	url = encodeURI(url);

	if(req!=null)
	{
		req.onreadystatechange = Process_car_request;
		req.open("GET", url, true);
		req.send(null);

	}


}


function save_car()
{

	var start = new Date();
	var tm=start.getTime();

	Initialize_dc(); 
	var auto_id = document.forms['car_form'].id.value ;
	var mark = document.forms['car_form'].mark.value ;
	var kirjeldus = document.forms['car_form'].kirjeldus.value ;
	var hind = document.forms['car_form'].hind.value ;
	var valjalase = document.forms['car_form'].valjalase.value ;

	var url=appserver_url + "AutoUpdateService?auto="+auto_id+"&mark="+ mark + "&kirjeldus="+ kirjeldus + "&hind=" + hind + "&valjalase=" + valjalase +"&tm="+tm ;
	// alert(url);
	url = encodeURI(url);

	if(req!=null)
	{
		req.onreadystatechange = Process_car_update;
		req.open("GET", url, true);
		req.send(null);

	}


}


function get_car_list()
{



	Initialize_dc(); 
	var mark = document.forms['autode_valiku_vorm'].mark.value;
	var url=appserver_url + "AutoListService?mark="+mark;
	url = encodeURI(url);

	if(req!=null)
	{
		req.onreadystatechange = Process_car_list_request;
		req.open("GET", url, true);
		req.send(null);

	}


}


function Process_car_request()
{
	var x;

	if (req.readyState == 4)
	{


		if (req.status == 200)
		{
			if(req.responseText=="")
			{ x = 1 ; }
			else
			{   


				//  parseXMLResult(req.responseText);

				parseXMLResult(req.responseXML);
				// ShowDiv_dc(my_divid);

			}
		}
		else
		{
			document.getElementById("rent_form_xml").innerHTML=
				"Oli mingi probleem andmete saamisega:<br>"+req.statusText;
		}
	}


}


function Process_car_list_request()
{
	var x;

	if (req.readyState == 4)
	{


		if (req.status == 200)
		{
			if(req.responseText=="")
			{ x = 1 ; }
			else
			{   

				document.getElementById("auto_valik").innerHTML=req.responseText

			}
		}
		else
		{
			document.getElementById("auto_valik").innerHTML=
				"Oli mingi probleem andmete saamisega:<br>"+req.statusText;
		}
	}


}


function Process_car_update()
{
	var x;

	if (req.readyState == 4)
	{


		if (req.status == 200)
		{
			{   

				document.getElementById("car_form_response").innerHTML=req.responseText;


			}
		}
		else
		{
			document.getElementById("car_form_response").innerHTML=
				"Oli mingi probleem andmete saamisega:<br>"+req.statusText;
		}
	}


}



function Process_rent_request()
{
	var x;

	if (req.readyState == 4)
	{


		if (req.status == 200)
		{

			parseRentXMLResult(req.responseXML);


		}
		else
		{
			document.getElementById("mutabel").innerHTML=
				"<tr><td>Oli mingi probleem andmete saamisega:<br>"+req.statusText+"</td></tr>";
		}
	}


}



