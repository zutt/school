<%-- 

 Main view generator
 Author - Martin

--%> 



<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
   
<%@ page import="java.util.List,kliendid.hibernate.model.Customer" %>
<%@ page import="kliendid.model.User" %>
<%@ page import="kliendid.utils.StaticUtils" %>
<jsp:useBean id="nimekiri" scope="request" type="List<Customer>" />
<jsp:useBean id="filter" scope="request" type="kliendid.model.SearchFilter" />


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="static/css/view.css" /> 
		<title>Select Customers t095116 &amp; t111053</title>
		<script type="text/javascript" src="static/js/main.js"></script>
		<script type="text/javascript" src="static/js/validate.js"></script>
	</head>
<body>
	<img id="top" src="static/picture/top.png" alt="">
	<div id="form_container">
	

		<form id="form_814877" class="appnitro"  method="post" action="">
			<div class="form_description">
				<h2>Edit Customers</h2>
				<p>You are at the Main menu | <a href='hr-mp-kliendid/logs/clientslog.txt'>logi</a> </p>
				
				<div align="right">
	 				<b>Username:</b> <%=((User)request.getSession().getAttribute("userData")).getUserName()%>
	 				<br><a href='login?s=logout'>logout</a>
				</div>
			</div>
			<table>
				<tr>
					<th>First</th>
					<td><input type="text" name="fltFirst" value="<%=filter.getFirstName() %>"/></td>
					<th>Last</th>
					<td><input type="text" name="fltLast" value="<%=filter.getLastName() %>"/></td>
				</tr>
				<tr>
					<th><input type="submit" value="Search"></th>
					<th><a href="changeCustomer">Add New Customer</a></th>
				</tr>
			</table>		

			<table border="1">
				<thead>
					<th>First</th>		
					<th>Last</th>		
					<th>ID code</th>
					<th>Notes</th>
					<th>Notes2</th>
					<th>Birthday</th>
					<th>Change</th>
					
				</thead>
				<!-- TODO: uued väljad ja edit nupud teistele väljadele  -->
				<tbody>
				<% 
					for (int i=0; i<nimekiri.size(); i++) { %>
			
					<tr>
						<td><a href="javascript:show_customer_nr(<%=nimekiri.get(i).getCustomer().toString()%>)"><%=nimekiri.get(i).getFirstName() %></a></td>
						<td><%=nimekiri.get(i).getLastName() %></td>
						<td><%=nimekiri.get(i).getIdentityCode() %></td>
						<td><div id="note_<%=i+1%>" style="visibility: hidden;"><%=nimekiri.get(i).getNote() %></div></td>
						<td>
							<div id="customer_note_<%=i+1%>"><A HREF="javascript:show_customer_note(<%=i+1%>)">ShowNote</a></div>
							
						</td>
						
						<td><%=StaticUtils.DateToString(nimekiri.get(i).getBirthDate()) %></td>
						<td><a href="changeCustomer?customer=<%=nimekiri.get(i).getCustomer()%>">change</a></td>
					</tr>
				<%; } ;%>
				</tbody>
			</table>
			<p>
				<button id="showme_button" type="button" onclick="showme('bottomform')">N&auml;ita vomi</button>
				<button id="hideme_button" type="button" onclick="hideme('bottomform')">Peida vorm</button>
			</p>
			<p> 
			First : <span id="NamelH1"></span> <br> 
			Last : <span id="HostelH1"></span> <br> 
			Code : <span id="ContactH1"></span> <br>
			</p>
							

		</form>	

	</div>
	
	<img id="bottom" src="static/picture/bottom.png" alt="">
	
	
	<div id="bottomform">
		<form class="appnitro">
			<table>
				<tbody>
					<tr id="li_0" >
						<td>
							Customer id: </td>
							<td><input id="customer_id" name="customer_id" class="element text medium" type="text" maxlength="255" disabled />
						</td> 
					</tr>
					<tr id="li_1" >
						<td>
							First name: </td>
							<td><input id="first_name" name="first_name" class="element text medium" type="text" maxlength="255"  />
						</td> 
					</tr>		
					<tr id="li_2" >
						<td>
							Last name:  </td>
							<td><input id="last_name" name="last_name" class="element text medium" type="text" maxlength="255"  />
						</td> 
					</tr>		
					<tr id="li_3" >
						<td>
							ID code:  </td>
							<td><input id="identity_code" name="identity_code" class="element text medium" type="text" maxlength="255"  />
							<div style="font-size:0.3em;">Length 4, number please</div>
						</td> 
					</tr>
					<tr id="li_3" >
						<td>
							Birthday:  </td>
							<td><input id="birthday" name="birthday" class="element text medium" type="text" maxlength="255"  />
						</td> 
					</tr>
					<tr>
						<td colspan="2">		    
							<button id="hideme_button2" type="button" onclick="hideme('bottomform')">Peida vorm</button>
							<button id="saveForm" type="button" name="saveForm" onClick="save_customer()">Save changes</button>
						</td>
					</tr>
	
				</tbody>
			</table>
		</form>
	</div>
	

</body>
</html>

