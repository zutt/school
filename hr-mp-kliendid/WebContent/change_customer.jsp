<%-- 

 Main view generator
 Owner - Hellereet

--%> 


<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="kliendid.model.CustomerAddressForm" %>
<%@ page import="kliendid.model.CustomerCommDeviceForm" %>
<%@ page import="kliendid.model.User" %>
<jsp:useBean id="vastus" scope="request" type="kliendid.model.CustomerForm" />
<jsp:useBean id="activeTab" scope="request" type="java.lang.Long" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript" src="static/js/main.js"></script>
<script type="text/javascript" src="static/js/validate.js"></script>
<!-- Helleka kood tabide naitamiseks/--> 

<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
  <!-- <link rel="stylesheet" href="/resources/demos/style.css"/-->
  
  <script>
  
	  $(function() {
	    
		 $( "#tabs" ).tabs({ active: <%=activeTab%> });
	    
	     $( "#addressDialog" ).dialog({
	    	 autoOpen: false,
	    	 modal: true
	     });
	     
	     $( "#deviceDialog" ).dialog({
	    	 autoOpen: false,
	    	 modal: true
	     });
	     
	     $( "#addNewAddress" ).click(function() {
	    	 
	    	 $( "#dd_address_id" ).val("");
	    	 $( "#dd_primary" ).val("");
         	 $( "#dd_zip" ).val("");
         	 $( "#dd_house" ).val("");
         	 $( "#dd_street" ).val("");
         	 $( "#dd_town" ).val("");
         	 $( "#dd_county" ).val("");
	    	 
         	 $( "#addressDialog" ).dialog("open");
	     });
	     
	     $( "#addNewDevice" ).click(function() {
	    	 
	    	 $( "#dd_device_id" ).val("");
         	 $( "#dd_device_type" ).val("1");
         	 $( "#dd_description" ).val("");
	    	 
         	 $( "#deviceDialog" ).dialog("open");
	     });
	     
	  });
  
	  function openAddress(addressID){
		  
		  $.ajax({

            url : "changeCustomer?s=readAddress&adrId=" + addressID ,
            dataType : 'json',
            type: 'GET',
            error : function() {

                alert("Error Occured");
            },
            success : function(data) {
            	
            	$( "#dd_address_id" ).val(data["address_id"]);
            	$( "#dd_zip" ).val(data["zip"]);
            	$( "#dd_house" ).val(data["house"]);
            	$( "#dd_street" ).val(data["street"]);
            	$( "#dd_town" ).val(data["town"]);
            	$( "#dd_county" ).val(data["county"]);

            }
        });
	
		$( "#addressDialog" ).dialog("open");
	  }
  
	  function openDevice(deviceID){
		  
		   $.ajax({

            url : "changeCustomer?s=readDevice&devId=" + deviceID ,
            dataType : 'json',
            type: 'GET',
            error : function() {

                alert("Error Occured");
            },
            success : function(data) {
            	
            	$( "#dd_device_id" ).val(data["device_id"]);
            	$( "#dd_device_type" ).val(data["device_type"]);
            	$( "#dd_description" ).val(data["description"]);
            }
          });
		  
		 $( "#deviceDialog" ).dialog("open");
	  }
  
  </script>
  
 <link rel="stylesheet" type="text/css" href="static/css/view.css" >
 
 <title>Customer Editor</title>

</head>
<body>

	
	<img id="top" src="static/picture/top.png" alt="">
	<div id="form_container">

		<div class="form_description">
			<h2>Edit Customers
			
			</h2>
			<p><a href="/hr-mp-kliendid/hr">Back to Main menu</a> | <a href='hr-mp-kliendid/logs/clientslog.txt'>log.txt</a> </p>
			<div align="right">
	 				<b>Username:</b> <%=((User)request.getSession().getAttribute("userData")).getUserName()%>			
					<br><a href='login?s=logout'>logout</a>
				</div>
		</div>	
		
		<div id="tabs">
			<ul>
		        <li><a href="#tabs-1">Customer Data</a></li>
		        <li><a href="#tabs-2">Addresses</a></li>
		        <li><a href="#tabs-3">Communication devices</a></li>
		        <li><a href="#tabs-4">Groups</a></li>
		    </ul>
   		
		<form id="form_814877"  method="POST" action="changeCustomer?s=saveGeneral">
	   		<div id="tabs-1">
				<div id="t1_content" style="background-color:#EEEEEE;">
					
						<fieldset>
						<legend>Customer's main data:</legend>
						<table border="0">
								<tbody>
									<tr>
										<td>
											Customer id: 
										</td>
										<td><%=(vastus.getCustomer()!=null)?vastus.getCustomer():"" %>
											<input type="hidden" name="customer_id" value="<%= (vastus.getCustomer()!=null)?vastus.getCustomer():"" %>" />
										</td> 
									</tr>
									<tr>
										<td>
											First name: 
										</td>
										<td>
											<input id="customer_firstname" name="customer_firstname" class="element text medium" type="text" maxlength="255" value="<%= (vastus.getFirstName()!=null)?vastus.getFirstName():"" %>"/> 
										</td> 
									</tr>		
									<tr>
										<td>
											Last name:  
										</td>
										<td>
											<input id="customer_lastname" name="customer_lastname" class="element text medium" type="text" maxlength="255" value="<%= (vastus.getLastName()!=null)?vastus.getLastName():"" %>"/>
										</td> 
									</tr>		
									<tr>
										<td>
											ID code:  
										</td>
										<td>
											<input id="customer_idcode" name="customer_idcode" class="element text medium" type="text" maxlength="255" value="<%= (vastus.getIdentityCode()!=null)?vastus.getIdentityCode():"" %>"/> 
										</td> 
									</tr>
									<tr>
										<td>
											Birthday:
										</td>
										<td>
											<input id= "birthday" name="birthday" type="text" size="10" value="<%= (vastus.getBirthDate()!=null)?vastus.getBirthDate():""%>"/>
										</td> 
									</tr>
									<tr>
										<td>
											Notes:  
										</td>
										<td>
											<textarea id= "note" name="note" style="width: 100px; height: 55px"><%= (vastus.getNote()!=null)?vastus.getNote():""%></textarea>
										</td>
									</tr>
									</tbody>
							</table>
							<div id="t1_content2" style="float:left;">
								<table>
									<tbody>
										<tr>
											<td>
												Created: 
											</td>
											<td>
												<%= (vastus.getCreated()!=null)?vastus.getCreated():""%>
											</td> 
										</tr>
										<tr>
											<td>
												Created By: 
											</td>
											 <td>
											 	<%= (vastus.getCreatedBy()!=null)?vastus.getCreatedBy():"" %>
											</td> 
										</tr>
										<tr>
											<td>
												Updated: 
											</td>
											<td>
												<%= (vastus.getUpdated()!=null)?vastus.getUpdated():""%>
											</td> 
										</tr>
										<tr>
											<td>
												Updated By: 
											</td>
											<td>
												<%= (vastus.getUpdatedBy()!=null)?vastus.getUpdatedBy():"" %>
											</td> 
										</tr>
									</tbody>
								</table>
							</div>
						</fieldset>
						<!-- Peamine aadress  -->
						<fieldset>
						<legend>Customer's main address:</legend>
						<table>
							<tbody>
									<input type="hidden" name="address_id" value="<%= (vastus.getPrimaryAddress() != null)?vastus.getPrimaryAddress().getAddressId():"" %>" />
									<tr>
										<td>
											ZIP code: 
										</td>
										<td>
											<input id="zip" name="zip" class="element text medium" type="text" maxlength="255" value="<%= (vastus.getPrimaryAddress() != null)?vastus.getPrimaryAddress().getZip():"" %>"/>
										</td> 
									</tr>
									<tr>
										<td>
											House nr: 
										</td>
										<td>
											<input id="house" name="house" class="element text medium" type="text" maxlength="255" value="<%= (vastus.getPrimaryAddress() != null)?vastus.getPrimaryAddress().getHouse():"" %>"/>
										</td> 
									</tr>
									<tr>
										<td>
											Street: 
										</td>
										<td>
											<input id="street" name="street" class="element text medium" type="text" maxlength="255" value="<%= (vastus.getPrimaryAddress() != null)?vastus.getPrimaryAddress().getStreet():"" %>"/>
										</td> 
									</tr>
									<tr>
										<td>
											Town: 
										</td>
										<td>
											<input id="town" name="town" class="element text medium" type="text" maxlength="255" value="<%= (vastus.getPrimaryAddress() != null)?vastus.getPrimaryAddress().getTown():"" %>"/>
										</td> 
									</tr>
									<tr>
										<td>
											County: 
										</td>
										<td>
											<input id="county" name="county" class="element text medium" type="text" maxlength="255" value="<%= (vastus.getPrimaryAddress() != null)?vastus.getPrimaryAddress().getCounty():"" %>"/>
										</td> 
									</tr>
							</tbody>
						</table>		
						</fieldset>
						<button type="submit">Save changes</button>
						<button type="button" onclick="window.location='changeCustomer?s=deleteCustomer&customer=<%=vastus.getCustomer() %>'">Delete</button>
					</div>
				</div>
			</form>
        
	    <div id="tabs-2">
	    	<fieldset>
					<legend>Customer's additional addresses:</legend>
	        <table frame = border rules="all">
				<thead>
					<tr>
						<th>ZIP</th>		
						<th>House</th>		
						<th>Street</th>
						<th>Town</th>
						<th>County</th>
						<th colspan="3"></th>
					</tr>
				</thead>
				<tbody>
				<% for (CustomerAddressForm address : vastus.getAddresses()) { %>
					<tr>
							<td><%= address.getZip() %></td>
							<td><%= address.getHouse() %></td>
							<td><%= address.getStreet() %></td>
							<td><%= address.getTown() %></td>
							<td><%= address.getCounty() %></td>							
							<td><a href="#" onclick="openAddress(<%= address.getAddressId() %>);"><img style="border:0;" src="static/picture/edit.jpg" alt="Edit" width="22" height="22"></a></td>
							<td><a href="changeCustomer?s=deleteAddress&adrId=<%= address.getAddressId() %>&customer=<%=vastus.getCustomer() %>"><img style="border:0;" src="static/picture/delete.jpg" alt="Delete" width="22" height="22"></a></td>
							
					</tr>
				<% } %>
				
				</tbody>
			</table>
			<a href="#" id="addNewAddress"><img style="border:0;" src="static/picture/button_add.jpg" alt="Add Additional Address" width="22" height="22">Add new address</a>
	        
			</fieldset>
	    </div>
	    <div id="tabs-3">
			<fieldset>
					<legend>Customer's communication devices:</legend>
					<table frame = border rules="all">
						<thead>
							<tr>
								<th>Device Type</th>
								<th>Description</th>
								<th colspan="3"></th>
							</tr>
						</thead>
						<tbody>
						<% for (CustomerCommDeviceForm device : vastus.getCommDevices()) { %>
							<tr>
			 						<td><%= device.getTypeName() %></td>
									<td><%= device.getDescription() %></td>
									<td><a href="#" onclick="openDevice(<%= device.getDeviceId() %>);"><img style="border:0;" src="static/picture/edit.jpg" alt="Edit" width="22" height="22"></a></td>
									<td><a href="changeCustomer?s=deleteDevice&devId=<%= device.getDeviceId() %>&customer=<%=vastus.getCustomer() %>"><img style="border:0;" src="static/picture/delete.jpg" alt="Delete" width="22" height="22"></a></td>
							</tr>
						<% } %>								
						</tbody>
					</table>
					<a href="#"  id="addNewDevice"><img style="border:0;" src="static/picture/button_add.jpg" alt="Add" width="22" height="22">Add New Communication Device</a>		
			</fieldset>
	    </div>
	    <form id="form_814877" method="POST" action="changeCustomer?s=saveGroup">
	    	<input type="hidden" name="customer_id" value="<%=vastus.getCustomer() %>" />
		    <div id="tabs-4">
		        <fieldset>
						<legend>Customer's belonging to Group(s):</legend>
						<table border="0">
							<thead>
								<tr>
									<th colspan=2>Select groups:</th>
								</tr>
							</thead>
							<tbody>
								<tr>
			 						<td><input type="checkbox" id="cgSingle" name="cgSingle" <%=vastus.getSingleGrp() %> ></td>
			 						<td>Single</td>
								</tr>	
								<tr>
			 						<td><input type="checkbox" id="cgMarried" name="cgMarried" <%=vastus.getMarriedGrp() %> ></td>
			 						<td>Married</td>
								</tr>
								<tr>
			 						<td><input type="checkbox" id="cgDivorced" name="cgDivorced" <%=vastus.getDivorcedGrp() %> ></td>
			 						<td>Divorced</td>
								</tr>
								<tr>
			 						<td><input type="checkbox" id="cgHappy" name="cgHappy" <%=vastus.getHappyGrp() %> ></td>
			 						<td>Happy</td>
								</tr>
								<tr>
			 						<td><input type="checkbox" id="cgUnhappy" name="cgUnhappy"  <%=vastus.getUnhappyGrp() %> ></td>
			 						<td>Unhappy</td>
								</tr>
							</tbody>
						</table>		
				</fieldset>
				
				<button type="submit">Save changes</button>
		    </div>
	    </form>	
	  </div>
</div>
<img id="bottom" src="static/picture/bottom.png" alt="">

<div id="addressDialog" title="Address">
	<form method="POST" action="changeCustomer?s=saveAddress">
		<table>
			<tbody>
					<input type="hidden" id="customer_id" name="customer_id" value="<%=vastus.getCustomer() %>" />
					<input type="hidden" id="dd_address_id" name="dd_address_id" value="" />
					<tr>
						<td>
							Primary address: 
						</td>
						<td>
							<input id="dd_primary" name="dd_primary" class="element text medium" type="checkbox"/>
						</td> 
					</tr>
					<tr>
						<td>
							ZIP code: 
						</td>
						<td>
							<input id="dd_zip" name="dd_zip" class="element text medium" type="text" maxlength="255" value=""/>
						</td> 
					</tr>
					<tr>
						<td>
							House nr: 
						</td>
						<td>
							<input id="dd_house" name="dd_house" class="element text medium" type="text" maxlength="255" value=""/>
						</td> 
					</tr>
					<tr>
						<td>
							Street: 
						</td>
						<td>
							<input id="dd_street" name="dd_street" class="element text medium" type="text" maxlength="255" value=""/>
						</td> 
					</tr>
					<tr>
						<td>
							Town: 
						</td>
						<td>
							<input id="dd_town" name="dd_town" class="element text medium" type="text" maxlength="255" value=""/>
						</td> 
					</tr>
					<tr>
						<td>
							County: 
						</td>
						<td>
							<input id="dd_county" name="dd_county" class="element text medium" type="text" maxlength="255" value=""/>
						</td> 
					</tr>
					<tr>
						<td colspan="2"><button type="submit">Save changes</button></td>
					</tr>
			</tbody>
		</table>	
	</form>
</div>

<div id="deviceDialog" title="Device">
	<form method="POST" action="changeCustomer?s=saveDevice">
		<table>
			<tbody>
					<input type="hidden" id="customer_id" name="customer_id" value="<%=vastus.getCustomer() %>" />
					<input type="hidden" id="dd_device_id" name="dd_device_id" value="" />
					<tr id="li_10" >
						<td>Type: </td>
						<td>
							<select id="dd_device_type" name="dd_device_type">
								<option value="1" selected>phone</option>
								<option value="2">e-mail</option>
								<option value="3">fax</option>
							</select>
						</td> 
					</tr>
					<tr>
						<td>
							Description: 
						</td>
						<td>
							<input id="dd_description" name="dd_description" class="element text medium" type="text" maxlength="255" value=""/>
						</td> 
					</tr>
					<tr>
						<td colspan="2"><button type="submit">Save changes</button></td>
					</tr>
			</tbody>
		</table>
	</form>
</div>
</body>
</html>