package kliendid.controller;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import kliendid.hibernate.dao.impl.CustomerDAOImpl;
import kliendid.model.SearchFilter;

public class MainServlet extends HttpServlet {
	
	static Logger logger = Logger.getLogger(MainServlet.class);

	
	private static final long serialVersionUID = 1L;

	public void init(ServletConfig config) throws ServletException {

		// log4j initsialiseerimine
		ServletContext context = config.getServletContext();
		System.setProperty("rootPath", context.getRealPath("/"));

		String log4jConfigFile = "WEB-INF/classes/log4j.properties";

		PropertyConfigurator.configure(context.getRealPath("") + File.separator
				+ log4jConfigFile);

		super.init(config);

		logger.info("Mind loodi");

	}

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MainServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		kliendid.hibernate.dao.CustomerDAO custDao = new CustomerDAOImpl();

		request.setAttribute("nimekiri", custDao.findCustomers(null));
		request.setAttribute("filter", new SearchFilter());

		request.getRequestDispatcher("main_clients.jsp").forward(request,
				response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		kliendid.hibernate.dao.CustomerDAO custDao = new CustomerDAOImpl();
		
		SearchFilter filter = new SearchFilter(request.getParameter("fltFirst"), request.getParameter("fltLast"));

		request.setAttribute("nimekiri", custDao.findCustomers(filter));
		request.setAttribute("filter", filter);
		
		request.getRequestDispatcher("main_clients.jsp").forward(request,
				response);
	}

}
