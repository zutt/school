/**
 * 
 */
package kliendid.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author martinparoll
 *
 */
public class ConverterClass {
	
	public static String dateToString(Date date) {
		DateFormat df = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		String returnDate = df.format(date);
		return returnDate;

	}
	
	public static String floatToString(float f) {		

		String s = Float.toString(f);
		return s;

	}
	
	public static float StringToFloat(String s) {		
		
		float f = Float.parseFloat(s);
		return f;

	}
	

	
}
