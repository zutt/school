package kliendid.controller;

/**
 * Servlet implementation class AjaxTrial
 * @author martinparoll
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import kliendid.db.CustomerDAO;
import kliendid.hibernate.model.Customer;


public class AjaxTrial extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
	static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(AjaxTrial.class);
	public AjaxTrial() { super(); }
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		//response.getWriter().println("Hello world!");
	}
		
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		String customerId = request.getParameter("customer_id");
		//logger.info("action: " + action);
		
		PrintWriter out = response.getWriter();
		response.setContentType("text/xml");

		if (action.equals("postid")) {
			out.println(getResult(customerId));
		} else if (action.equals("update")) {
			//logger.info("Started update, customer: " + customerId);
			//var url=ajaxserver_url + "?action=update&customer_id"+customer_id+"&first_name="+ first_name + "&last_name="+ last_name + "&identity_code=" + identity_code + "&tm="+tm ;
			Customer customer = new Customer();
			String first_name = request.getParameter("first_name");
			String last_name = request.getParameter("last_name");
			String identity_code = request.getParameter("identity_code");
			Date updated = new Date();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			customer.setCustomer(new BigDecimal(customerId));
			customer.setFirstName(first_name);
			customer.setLastName(last_name);
			customer.setIdentityCode(identity_code);
			//TODO - may need converting to better format?
			customer.setUpdated(updated); 
			
			//Korralik kuupäeva ja kella formaat
			//logger.info("Prindime andmeid update jaoks " + dateFormat.format(updated) ); 
			logger.info(customer);
			
			CustomerDAO.updateCustomer(customer);
		
		} else {
			logger.error("Action chose is not defined for: " + action);
		}
		
	}
	
	public String getResult(String customerId){
		Customer customer = new Customer();
		customer = CustomerDAO.getCustomerByNr(customerId);
		
		String first_name = customer.getFirstName();
		String last_name = customer.getLastName();
		String identity_code = customer.getIdentityCode();
		//Date birthday = customer.getBirth_date();
		//String birthdayString = ConverterClass.dateToString(birthday);
		String result = "<Customers>";
		result += "<Customer>"; 
		result += "<CustomerID>" + customerId + "</CustomerID>";
		result += "<First>" + first_name + "</First>";
		result += "<Last>" + last_name + "</Last>";
		result += "<IDcode>" + identity_code + "</IDcode>";
		//result += "<BDay>" + birthdayString + "</BDay>";
		result += "</Customer>"; result += "</Customers>";		

		return result;
		
	}
	
	public String sendAjaxData(String customerId){
		Customer customer = new Customer();
		customer = CustomerDAO.getCustomerByNr(customerId);
		
		String first_name = customer.getFirstName();
		String last_name = customer.getLastName();
		String identity_code = customer.getIdentityCode();
		//Date birthday = customer.getBirth_date();
		//String birthdayString = ConverterClass.dateToString(birthday);
		String result = "<Customers>";
		result += "<Customer>"; 
		result += "<CustomerID>" + customerId + "</CustomerID>";
		result += "<First>" + first_name + "</First>";
		result += "<Last>" + last_name + "</Last>";
		result += "<IDcode>" + identity_code + "</IDcode>";
		//result += "<BDay>" + birthdayString + "</BDay>";
		result += "</Customer>"; result += "</Customers>";		

		return result;
		
	}
}