package kliendid.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import kliendid.hibernate.dao.CGroupDAO;
import kliendid.hibernate.dao.CommDeviceDAO;
import kliendid.hibernate.dao.CommDeviceTypeDAO;
import kliendid.hibernate.dao.CstAddressDAO;
import kliendid.hibernate.dao.CustomerDAO;
import kliendid.hibernate.dao.CustomerGroupDAO;
import kliendid.hibernate.dao.EmployeeDAO;
import kliendid.hibernate.dao.impl.CGroupDAOImpl;
import kliendid.hibernate.dao.impl.CommDeviceDAOImpl;
import kliendid.hibernate.dao.impl.CommDeviceTypeDAOImpl;
import kliendid.hibernate.dao.impl.CstAddressDAOImpl;
import kliendid.hibernate.dao.impl.CustomerDAOImpl;
import kliendid.hibernate.dao.impl.CustomerGroupDAOImpl;
import kliendid.hibernate.dao.impl.EmployeeDAOImpl;
import kliendid.hibernate.model.CommDevice;
import kliendid.hibernate.model.CstAddress;
import kliendid.hibernate.model.Customer;
import kliendid.hibernate.model.CustomerGroup;
import kliendid.hibernate.model.Employee;
import kliendid.model.CustomerForm;
import kliendid.model.User;
import kliendid.utils.GroupsEnum;
import kliendid.utils.StaticUtils;

/**
 * Servlet implementation class ChangeCustomer
 * @author Martin
 */
public class ChangeCustomer extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(ChangeCustomer.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChangeCustomer() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//ajaxi p�ringud
		String s = request.getParameter("s");
		
		if("readAddress".equals(s)){
			
			String adrId = request.getParameter("adrId");
			
			CstAddressDAO adrDao = new CstAddressDAOImpl();
			CstAddress address =  adrDao.findById(new BigDecimal(adrId));
			
	        response.setContentType("application/json;charset=utf-8");

	        PrintWriter pw = response.getWriter(); 
	        pw.print(address.toJsonString());
	        pw.close();
			
		}else if ("readDevice".equals(s)){
			
			String devId = request.getParameter("devId");
			
			CommDeviceDAO devDao = new CommDeviceDAOImpl();
			CommDevice device =  devDao.findById(new BigDecimal(devId));
			
	        response.setContentType("application/json;charset=utf-8");

	        PrintWriter pw = response.getWriter(); 
	        pw.print(device.toJsonString());
	        pw.close();
	        
		}else{
			
			String id = request.getParameter("customer");
			long activeTab = 0;
			
			if(id != null){
				
				CustomerDAO custDao = new CustomerDAOImpl();
				Customer customer = custDao.findById(new BigDecimal(id));
				
				if ("deleteAddress".equals(s)){
					
					String adrId = request.getParameter("adrId");
					CstAddressDAO adrDao = new CstAddressDAOImpl();
					CstAddress address =  adrDao.findById(new BigDecimal(adrId));
					customer.getCstAddresss().remove(address);
					adrDao.deleteEntity(address);
					
					activeTab = 1;
					
				}else if("deleteDevice".equals(s)){
					
					String devId = request.getParameter("devId");
					CommDeviceDAO devDao = new CommDeviceDAOImpl();
					CommDevice device =  devDao.findById(new BigDecimal(devId));
					customer.getCommDevices().remove(device);
					devDao.deleteEntity(device);
					
					activeTab = 2;
					
				}else if("deleteCustomer".equals(s)){
					
					kliendid.db.CustomerDAO.deleteCustomer(id);
					response.sendRedirect("hr");
					return;
				}
				request.setAttribute("activeTab", activeTab);
				request.setAttribute("vastus", new CustomerForm(customer));
			}else{
				request.setAttribute("activeTab", activeTab);
				request.setAttribute("vastus", new CustomerForm());
			}
			
			request.getRequestDispatcher("change_customer.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		request.setCharacterEncoding("UTF-8");
		
		String s = request.getParameter("s");
		
		BigDecimal id = null;
		long activeTab = 0;
		
		if("saveGeneral".equals(s)){
			id = saveGeneralData(request, response);
			activeTab = 0;
		}else if("saveGroup".equals(s)){
			id = saveGroupData(request, response);
			activeTab = 3;
		}else if("saveAddress".equals(s)){
			id = saveAddress(request, response);
			activeTab = 1;
		}else if("saveDevice".equals(s)){
			id = saveDevice(request, response);
			activeTab = 2;
		}
	
		CustomerDAO custDao = new CustomerDAOImpl();
		Customer customer = custDao.findById(id);
		
		request.setAttribute("activeTab", activeTab);
		request.setAttribute("vastus", new CustomerForm(customer));
		request.getRequestDispatcher("change_customer.jsp").forward(request, response);
	}
	
	private BigDecimal saveGeneralData(HttpServletRequest request, HttpServletResponse response){
		
		/*
		 * Customer-i andmed
		 */
		String id = request.getParameter("customer_id");
		String firstName = request.getParameter("customer_firstname");
		String lastName = request.getParameter("customer_lastname");
		String idCode = request.getParameter("customer_idcode");
		Date birthDay = StaticUtils.StringToDate(request.getParameter("birthday"));
		String note = request.getParameter("note");
		
		User acUser = (User)request.getSession().getAttribute("userData");
		EmployeeDAO empDao = new EmployeeDAOImpl();
		Employee emp = empDao.findById(acUser.getUserId());
		Date upinDate = new Date();
		
		CustomerDAO custDao = new CustomerDAOImpl();
		Customer customer = null;
		
		if(id == null || id.trim().length() == 0){
			customer = new Customer();
			customer.setCreatedBy(emp);
			customer.setCreated(upinDate);
			
		}else{
			customer = custDao.findById(new BigDecimal(id));
			customer.setUpdatedBy(emp);
			customer.setUpdated(upinDate);
		}
		
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setIdentityCode(idCode);
		customer.setBirthDate(birthDay);
		customer.setNote(note);
		
		//uuendab baasis kirjet
		custDao.saveOrUpdateEntity(customer);

		/*
		 * Pohiaadressi andmed
		 */
		String addressId =  request.getParameter("address_id");
		String zip = request.getParameter("zip");
		String house = request.getParameter("house");
		String street = request.getParameter("street");
		String town = request.getParameter("town");
		String county = request.getParameter("county");
		
		CstAddressDAO cstAddressDao = new CstAddressDAOImpl();
		CstAddress primaryAddress = null;
		
		if(addressId == null || addressId.trim().length() == 0){
			primaryAddress = new CstAddress();
			customer.getCstAddresss().add(primaryAddress);
		}else
			primaryAddress = cstAddressDao.findById(new BigDecimal(addressId));
			
		primaryAddress.setZip(zip);
		primaryAddress.setHouse(house);
		primaryAddress.setAddress(street);
		primaryAddress.setTownCounty(town);
		primaryAddress.setCounty(county);
		primaryAddress.setCustomer(customer);
		primaryAddress.setAddressType(new BigDecimal(1));

		cstAddressDao.saveOrUpdateEntity(primaryAddress);
		
		return customer.getCustomer();
		
	}
	
	private BigDecimal saveGroupData(HttpServletRequest request, HttpServletResponse response){

		String id = request.getParameter("customer_id");
		CustomerDAO custDao = new CustomerDAOImpl();
		Customer customer = custDao.findById(new BigDecimal(id));

		CustomerGroupDAO custGrpDao = new CustomerGroupDAOImpl();
		CGroupDAO cgrDao = new CGroupDAOImpl();

		/*
		 * Gropus
		 */
		String cgSingle =  request.getParameter("cgSingle");
		saveGroup(GroupsEnum.SINGLE, custGrpDao, cgrDao, cgSingle, customer);
		
		String cgMarried = request.getParameter("cgMarried");
		saveGroup(GroupsEnum.MARRIED, custGrpDao, cgrDao, cgMarried, customer);
		
		String cgDivorced = request.getParameter("cgDivorced");
		saveGroup(GroupsEnum.DIVORCED, custGrpDao, cgrDao, cgDivorced, customer);
		
		String cgHappy = request.getParameter("cgHappy");
		saveGroup(GroupsEnum.HAPPY, custGrpDao, cgrDao, cgHappy, customer);
		
		String cgUnhappy = request.getParameter("cgUnhappy");
		saveGroup(GroupsEnum.UNHAPPY, custGrpDao, cgrDao, cgUnhappy, customer);
		
		return new BigDecimal(id);

	}
	
	private void saveGroup(GroupsEnum group, CustomerGroupDAO custGrpDao, CGroupDAO cgrDao, String status, Customer customer){
		
		CustomerGroup singleGrp = customer.getGroup(group);
		
		if(status != null && singleGrp == null){
			
			CustomerGroup newGrp = new CustomerGroup();
			newGrp.setCustomer(customer);
			newGrp.setCGroup(cgrDao.findById(new BigDecimal(group.getId())));
			customer.getCustomerGroups().add(newGrp);
			custGrpDao.saveOrUpdateEntity(newGrp);
			
		}else if(status == null && singleGrp != null){
			
			customer.getCustomerGroups().remove(singleGrp);
			custGrpDao.deleteEntity(singleGrp);
			
		}
	}
	
	private BigDecimal saveAddress(HttpServletRequest request, HttpServletResponse response){
		
		String id = request.getParameter("customer_id");
		String primary = request.getParameter("dd_primary");
		String addressId =  request.getParameter("dd_address_id");
		String zip = request.getParameter("dd_zip");
		String house = request.getParameter("dd_house");
		String street = request.getParameter("dd_street");
		String town = request.getParameter("dd_town");
		String county = request.getParameter("dd_county");
		
		CustomerDAO custDao = new CustomerDAOImpl();
		Customer customer = custDao.findById(new BigDecimal(id));
		
		CstAddressDAO cstAddressDao = new CstAddressDAOImpl();
		
		//kui salvestatav aadress m��ratakse p�hiliseks, siis tuleb vana
		//p�hiline �ra muuta
		
		if(primary != null){
			CstAddress oldPrimaryAddress =  customer.getPrimaryAddress();
			oldPrimaryAddress.setAddressType(null);
			cstAddressDao.saveOrUpdateEntity(oldPrimaryAddress);
		}
		
		CstAddress address = null;
		
		if(addressId == null || addressId.trim().length() == 0){
			address = new CstAddress();
			customer.getCstAddresss().add(address);
		}else
			address = cstAddressDao.findById(new BigDecimal(addressId));
			
		address.setZip(zip);
		address.setHouse(house);
		address.setAddress(street);
		address.setTownCounty(town);
		address.setCounty(county);
		address.setCustomer(customer);
		address.setAddressType((primary != null)?new BigDecimal(1):null);

		cstAddressDao.saveOrUpdateEntity(address);
		
		return new BigDecimal(id);
	}
	
	private BigDecimal saveDevice(HttpServletRequest request, HttpServletResponse response){
		
		String id = request.getParameter("customer_id");
		String device_id =  request.getParameter("dd_device_id");
		String device_type = request.getParameter("dd_device_type");
		String description = request.getParameter("dd_description");
		
		CustomerDAO custDao = new CustomerDAOImpl();
		Customer customer = custDao.findById(new BigDecimal(id));
		
		CommDeviceDAO deviceDao = new CommDeviceDAOImpl();
		CommDeviceTypeDAO deviceTypeDao = new CommDeviceTypeDAOImpl();
		CommDevice device = null;
		
		if(device_id == null || device_id.trim().length() == 0){
			device = new CommDevice();
			customer.getCommDevices().add(device);
		}else
			device = deviceDao.findById(new BigDecimal(device_id));
			
		device.setValueText(description);
		device.setCustomer(customer);
		device.setCommDeviceType(deviceTypeDao.findById(new BigDecimal(device_type)));

		deviceDao.saveOrUpdateEntity(device);
		
		return new BigDecimal(id);
	}

}
