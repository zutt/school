package kliendid.controller;

import java.io.IOException;
import java.math.BigDecimal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kliendid.hibernate.dao.EmpUserDAO;
import kliendid.hibernate.dao.impl.EmpUserDAOImpl;
import kliendid.hibernate.model.EmpUser;
import kliendid.model.User;

import org.apache.log4j.Logger;

public class LoginServlet extends HttpServlet {

	static Logger logger = Logger.getLogger(LoginServlet.class);

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		if (request.getParameter("s") != null) {

			request.getSession().removeAttribute("userData");
			response.sendRedirect("login");

		} else {

			String userName = request.getParameter("userName");
			String password = request.getParameter("password");

			EmpUserDAO empUserDao = new EmpUserDAOImpl();

			EmpUser empUser = empUserDao.getEmployeeByUserNameAndPassword(
					userName, password);

			if (empUser != null) {

				HttpSession session = request.getSession(true);

				User user = new User();
				user.setUserId(empUser.getEmployee().getEmployee());
				user.setUserName(empUser.getEmployee().getName());
				session.setAttribute("userData", user);

			}

			response.sendRedirect("hr");
		}

	}

}
