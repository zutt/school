package kliendid.db;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import kliendid.hibernate.model.Customer;

/**
 * @author martinparoll
 * 
 */
public class CustomerDAO {

	static Logger logger = Logger.getLogger(CustomerDAO.class);

	public static Customer getCustomerByNr(float customer_nr) {
		String sql = "";
		java.sql.Connection myConnection = null;
		ResultSet queryResultsFromDB = null;
		Statement st = null;
		try {
			myConnection = dbconnection.getConnection();
			st = myConnection.createStatement();
			sql = "select "
					+ "customer, "
					+ "first_name, "
					+ "last_name, "
					+ "identity_code, "
					+ "note, "
					+ "birth_date, "
					+ "created, "
					+ "created_by, "
					+ "(select first_name || ' ' || last_name from employee where employee.employee = customer.created_by) created_by_name, "
					+ "updated, "
					+ "updated_by, "
					+ "(select first_name || ' ' || last_name from employee where employee.employee = customer.updated_by) updated_by_name "
					+ "from customer where customer = '"
					+ Math.round(customer_nr) + "'";
			// MainServlet.logThis("getCustomerByNr executed: " + sql);
			logger.info("getCustomerByNr executed: " + sql);
			queryResultsFromDB = st.executeQuery(sql);

			queryResultsFromDB.next();
			Customer customer_from_db = new Customer();
			customer_from_db.setCustomer(queryResultsFromDB
					.getBigDecimal("customer"));
			customer_from_db.setFirstName(queryResultsFromDB
					.getString("first_name"));
			customer_from_db.setLastName(queryResultsFromDB
					.getString("last_name"));
			customer_from_db.setIdentityCode(queryResultsFromDB
					.getString("identity_code"));
			customer_from_db.setNote(queryResultsFromDB.getString("note"));
			customer_from_db.setBirthDate(queryResultsFromDB
					.getDate("birth_date"));
			customer_from_db.setCreated(queryResultsFromDB.getDate("created"));
			// customer_from_db.setCreatedBy(queryResultsFromDB.getBigDecimal("created_by"));
			// customer_from_db.setCreatedByName(queryResultsFromDB.getString("created_by_name"));
			customer_from_db.setUpdated(queryResultsFromDB.getDate("updated"));
			// customer_from_db.setUpdatedByName(queryResultsFromDB.getString("updated_by_name"));
			// customer_from_db.setUpdatedBy(queryResultsFromDB.getBigDecimal("updated_by"));
			myConnection.close();
			// logger.info("closing db-connection");
			return customer_from_db;
		}

		catch (Exception ex) {
			// MainServlet.logThis("Error CustomerDAO getCustomerByNr :" +
			// ex.getMessage() + " CustomerNr=" + customer_nr );
			logger.error("Error CustomerDAO getCustomerByNr :"
					+ ex.getMessage() + " CustomerNr=" + customer_nr);

		} finally {
			dbconnection.closeStatement(st);
			dbconnection.closeResultSet(queryResultsFromDB);
			dbconnection.close(myConnection);
		}

		return null;
	}

	public static List<Customer> getAllCustomersFromDB() {
		String sql = "";
		java.sql.Connection myConnection = null;
		ResultSet queryResultsFromDB = null;
		Statement st = null;
		try {
			myConnection = dbconnection.getConnection();
			st = myConnection.createStatement();
			sql = "select customer, first_name, last_name, identity_code, note, birth_date, created, created_by, updated, updated_by from customer";
			// MainServlet.logThis("getAllCustomersFromDB executed: " + sql);
			logger.info("getAllCustomersFromDB executed: " + sql);
			queryResultsFromDB = st.executeQuery(sql);

			List<Customer> vastus = new ArrayList();

			while (queryResultsFromDB.next()) {
				Customer customer_from_db = new Customer();
				customer_from_db.setCustomer(queryResultsFromDB
						.getBigDecimal("customer"));
				customer_from_db.setFirstName(queryResultsFromDB
						.getString("first_name"));
				customer_from_db.setLastName(queryResultsFromDB
						.getString("last_name"));
				customer_from_db.setIdentityCode(queryResultsFromDB
						.getString("identity_code"));
				customer_from_db.setNote(queryResultsFromDB.getString("note"));
				customer_from_db.setBirthDate(queryResultsFromDB
						.getDate("birth_date"));
				customer_from_db.setCreated(queryResultsFromDB
						.getDate("created"));
				// customer_from_db.setCreatedBy(queryResultsFromDB.getBigDecimal("created_by"));
				customer_from_db.setUpdated(queryResultsFromDB
						.getDate("updated"));
				// customer_from_db.setUpdatedBy(queryResultsFromDB.getBigDecimal("updated_by"));
				myConnection.close();
				vastus.add(customer_from_db);
			}
			myConnection.close();
			return vastus;

		}

		catch (Exception ex) {
			// MainServlet.logThis("Error CustomerDAO getAllCustomersFromDB" +
			// ex.getMessage() );
			logger.error("Error CustomerDAO getAllCustomersFromDB"
					+ ex.getMessage());

		} finally {
			dbconnection.closeStatement(st);
			dbconnection.closeResultSet(queryResultsFromDB);
			dbconnection.close(myConnection);
		}
		return null;

	}

	// /*
	public static void updateCustomer(Customer customer) {
		String sql = "";
		java.sql.Connection myConnection = null;
		ResultSet queryResultsFromDB = null;
		PreparedStatement st = null;
		try {
			myConnection = dbconnection.getConnection();
			sql = "update customer set first_name = ?, last_name = ?, identity_code = ? "
					// TODO - kui esimesed andmed hakkavad salvestuma, siis
					// vaatan, mis nendega üldse teha
					// TODO - võibolla on vaja teha ajaxi jaoks eraldi
					// kliendiandmete updateCustomer
					// +
					// ", note = ?, birth_date = ?, created = ?, created_by = ?, updated = ?, updated_by = ?"
					+ "where customer=?";
			st = myConnection.prepareStatement(sql);
			st.setString(1, customer.getFirstName());
			st.setString(2, customer.getLastName());
			st.setString(3, customer.getIdentityCode());
			// TODO - kui esimesed andmed hakkavad salvestuma, siis vaatan, mis
			// nendega üldse teha
			// TODO - võibolla on vaja teha ajaxi jaoks eraldi kliendiandmete
			// updateCustomer

			/*
			 * st.setString(4, customer.getNote()); //date formaati ei saa otse
			 * sqli anda java.sql.Date sqlDateBirth = new
			 * java.sql.Date(customer.getBirth_date().getTime()); st.setDate(5,
			 * sqlDateBirth); java.sql.Date sqlDateCreated = new
			 * java.sql.Date(customer.getCreated().getTime()); st.setDate(6,
			 * sqlDateCreated); //float tuleb enne sqli castida longiks tundub
			 * st.setLong(7, (long) customer.getCreated_by()); java.sql.Date
			 * sqlDateUpdate = new
			 * java.sql.Date(customer.getUpdated().getTime()); st.setDate(8,
			 * sqlDateUpdate); st.setLong(9, (long) customer.getUpdated_by());
			 */

			// TODO set back to setLong(10) when entering more data
			st.setLong(4, customer.getCustomer().longValue());
			st.executeUpdate();
			// MainServlet.logThis("updateCustomer executed: " + sql);
			logger.info("updateCustomer executed: " + st);
			myConnection.close();

		}

		catch (Exception ex) {
			// MainServlet.logThis("Error CustomerDAO updateCustomer" +
			// ex.getMessage() );
			logger.error("Error CustomerDAO updateCustomer " + ex.getMessage());

		} finally {
			dbconnection.closeStatement(st);
			dbconnection.closeResultSet(queryResultsFromDB);
			dbconnection.close(myConnection);
		}

	}

	// */

	public static Customer getCustomerByNr(String customerId) {
		float f = Float.parseFloat(customerId);
		// String s = Float.toString(25.0f);
		return getCustomerByNr(f);
	}

	/**
	 * Kogu Customer'i andmete kustutamine
	 * @param customerId
	 */
	public static void deleteCustomer(String customerId) {

		String sql = null;

		java.sql.Connection myConnection = null;
		ResultSet queryResultsFromDB = null;
		PreparedStatement st = null;
		try {
			myConnection = dbconnection.getConnection();

			// kustutame grupid
			sql = "delete from customer_group where customer = ?";
			st = myConnection.prepareStatement(sql);
			st.setBigDecimal(1, new BigDecimal(customerId));
			st.execute();

			// kustutame aadressid
			sql = "delete from cst_address where customer = ?";
			st = myConnection.prepareStatement(sql);
			st.setBigDecimal(1, new BigDecimal(customerId));
			st.execute();

			// kustutame sidevahendid
			sql = "delete from comm_device where customer = ?";
			st = myConnection.prepareStatement(sql);
			st.setBigDecimal(1, new BigDecimal(customerId));
			st.execute();

			// kustutame customeri
			sql = "delete from customer where customer = ?";
			st = myConnection.prepareStatement(sql);
			st.setBigDecimal(1, new BigDecimal(customerId));
			st.execute();
			
			logger.info("deleteCustomer executed: " + st);
			myConnection.close();

		}catch (Exception ex) {
			logger.error("Error CustomerDAO deleteCustomer " + ex.getMessage());

		} finally {
			dbconnection.closeStatement(st);
			dbconnection.closeResultSet(queryResultsFromDB);
			dbconnection.close(myConnection);
		}

	}

}
