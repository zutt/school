package kliendid.utils;

public enum GroupsEnum {

	SINGLE("1"),
	MARRIED("2"),
	DIVORCED("3"),
	HAPPY("4"),
	UNHAPPY("5");
	
	private String id; 
	 
	GroupsEnum(String id) {
		this.id = id;
	}
	 
	public String getId() {
		return id; 
	}
}
