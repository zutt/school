package kliendid.utils;

/*
 * hr - ChangeCustomer - kasutab synnikuupaeva lihtformaati panemiseks
 */

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

public class StaticUtils {
	
	private static final Logger logger = Logger.getLogger(StaticUtils.class);
	
	private final static String dateFormatString = "dd.MM.yyyy";
	
	public static Date StringToDate(String str){
		
		Date date = null;
		
		if(str != null && str.trim().length() > 0){
			
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(dateFormatString);
				date = sdf.parse(str);
			} catch (Exception e) {
				
				logger.error(e);
			}
		}
		return date;
	}
	
	public static String DateToString(Date date) {

		if (date == null)
			return "";

		String str = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(dateFormatString);
			str = sdf.format(date);
		} catch (Exception e) {
			logger.error(e);
		}
		return str;
	}

}
