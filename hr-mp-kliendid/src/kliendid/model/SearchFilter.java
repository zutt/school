package kliendid.model;

public class SearchFilter {

	private String firstName;
	private String lastName;
	
	public SearchFilter(){
		
		firstName = "";
		lastName = "";
	}
	
	public SearchFilter(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}

	
}
