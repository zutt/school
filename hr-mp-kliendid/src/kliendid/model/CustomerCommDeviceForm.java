package kliendid.model;

import kliendid.hibernate.model.CommDevice;

public class CustomerCommDeviceForm {
	
	private String deviceId;
	private String typeId;
	private String typeName;
	private String description;
	
	public CustomerCommDeviceForm(){
		
	}
	
	public CustomerCommDeviceForm(CommDevice custCommDevice){
		
		this.deviceId = custCommDevice.getCommDevice().toString();
		this.typeId = custCommDevice.getCommDeviceType().getCommDeviceType().toString();
		this.typeName = custCommDevice.getCommDeviceType().getName();
		this.description = custCommDevice.getValueText();
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
