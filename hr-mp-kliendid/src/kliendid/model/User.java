package kliendid.model;

import java.math.BigDecimal;

public class User {
	
	private BigDecimal userId;
	private String userName;

	public BigDecimal getUserId() {
		return userId;
	}
	public void setUserId(BigDecimal userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
}
