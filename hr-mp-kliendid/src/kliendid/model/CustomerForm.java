package kliendid.model;

/*
 * Change Customer data hoidmise klass - siin hoiame kõiki andmeid, millega Tab'id täidetakse
 */

import java.util.ArrayList;
import java.util.List;

import kliendid.hibernate.model.CommDevice;
import kliendid.hibernate.model.CstAddress;
import kliendid.hibernate.model.Customer;
import kliendid.hibernate.model.CustomerGroup;
import kliendid.utils.GroupsEnum;
import kliendid.utils.StaticUtils;

public class CustomerForm {
	
	public CustomerForm(){
		
	}
	
	public CustomerForm(Customer cust){
		
		this.customer = cust.getCustomer().toString();
		this.firstName = cust.getFirstName();
		this.lastName = cust.getLastName();
		this.identityCode = cust.getIdentityCode();
		this.note = cust.getNote();
		this.created = StaticUtils.DateToString(cust.getCreated());
		this.createdBy = (cust.getCreatedBy() != null)?cust.getCreatedBy().getName():null;
		this.updated = StaticUtils.DateToString(cust.getUpdated());
		this.updatedBy = (cust.getUpdatedBy() != null)?cust.getUpdatedBy().getName():null;
		this.birthDate = StaticUtils.DateToString(cust.getBirthDate());
		
		if(cust.getPrimaryAddress() != null)
			this.primaryAddress = new CustomerAddressForm(cust.getPrimaryAddress());
		
		for(CstAddress address : cust.getCstAddresss()){
			
			if(address.getAddressType() == null)
				addresses.add(new CustomerAddressForm(address));
		}
		
		for(CommDevice device : cust.getCommDevices()){
			
			commDevices.add(new CustomerCommDeviceForm(device));
		}
		
		for(CustomerGroup custGroup : cust.getCustomerGroups()){
			
			if(GroupsEnum.SINGLE.getId().equals(custGroup.getCGroup().getCGroup().toString())){
				this.singleGrp = "checked";
			}else if(GroupsEnum.MARRIED.getId().equals(custGroup.getCGroup().getCGroup().toString())){
				this.marriedGrp = "checked";
			}else if(GroupsEnum.DIVORCED.getId().equals(custGroup.getCGroup().getCGroup().toString())){
				this.divorcedGrp = "checked";
			}else if(GroupsEnum.HAPPY.getId().equals(custGroup.getCGroup().getCGroup().toString())){
				this.happyGrp = "checked";
			}else if(GroupsEnum.UNHAPPY.getId().equals(custGroup.getCGroup().getCGroup().toString())){
				this.unhappyGrp = "checked";
			}
		}
		
	}

	private String customer;
	private String firstName;
	private String lastName;
	private String identityCode;
	private String note;
	private String created;
	private String updated;
	private String createdBy;
	private String updatedBy;
	private String birthDate;
	private CustomerAddressForm primaryAddress;
	private List<CustomerAddressForm> addresses = new ArrayList<CustomerAddressForm>();
	private List<CustomerCommDeviceForm> commDevices = new ArrayList<CustomerCommDeviceForm>();
	private String singleGrp;
	private String marriedGrp;
	private String divorcedGrp;
	private String happyGrp;
	private String unhappyGrp;
	
	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getIdentityCode() {
		return identityCode;
	}

	public void setIdentityCode(String identityCode) {
		this.identityCode = identityCode;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getUpdated() {
		return updated;
	}

	public void setUpdated(String updated) {
		this.updated = updated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	
	public CustomerAddressForm getPrimaryAddress() {
		return primaryAddress;
	}

	public void setPrimaryAddress(CustomerAddressForm primaryAddress) {
		this.primaryAddress = primaryAddress;
	}

	public List<CustomerAddressForm> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<CustomerAddressForm> addresses) {
		this.addresses = addresses;
	}

	public List<CustomerCommDeviceForm> getCommDevices() {
		return commDevices;
	}

	public void setCommDevices(List<CustomerCommDeviceForm> commDevices) {
		this.commDevices = commDevices;
	}

	public String getSingleGrp() {
		return singleGrp;
	}

	public void setSingleGrp(String singleGrp) {
		this.singleGrp = singleGrp;
	}

	public String getMarriedGrp() {
		return marriedGrp;
	}

	public void setMarriedGrp(String marriedGrp) {
		this.marriedGrp = marriedGrp;
	}

	public String getDivorcedGrp() {
		return divorcedGrp;
	}

	public void setDivorcedGrp(String divorcedGrp) {
		this.divorcedGrp = divorcedGrp;
	}

	public String getHappyGrp() {
		return happyGrp;
	}

	public void setHappyGrp(String happyGrp) {
		this.happyGrp = happyGrp;
	}

	public String getUnhappyGrp() {
		return unhappyGrp;
	}

	public void setUnhappyGrp(String unhappyGrp) {
		this.unhappyGrp = unhappyGrp;
	}

	
	/*
	 * Martin, kas sa seda allolevat osa kuskil kasutad?
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CustomerForm [customer=" + customer + ", first_name="
				+ firstName + ", last_name=" + lastName + ", identity_code="
				+ identityCode + ", note=" + note + ", created=" + created
				+ ", updated=" + updated + ", created_by=" + createdBy
				+ ", updated_by=" + updatedBy + ", birth_date=" + birthDate
				+ "]";
	}
}