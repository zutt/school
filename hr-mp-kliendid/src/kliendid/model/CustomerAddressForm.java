package kliendid.model;

import kliendid.hibernate.model.CstAddress;

public class CustomerAddressForm {
	
	private String addressId;
	private String zip;
	private String house;
	private String street;
	private String town;
	private String county;
	
	public CustomerAddressForm(){
		
	}
	
	public CustomerAddressForm(CstAddress custAddrees){
		
		this.addressId = custAddrees.getCstAddress().toString();
		this.zip = custAddrees.getZip();
		this.house = custAddrees.getHouse();
		this.street = custAddrees.getAddress();
		this.town = custAddrees.getTownCounty();
		this.county = custAddrees.getCounty();
	}
	
	public String getAddressId() {
		return addressId;
	}
	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getHouse() {
		return house;
	}
	public void setHouse(String house) {
		this.house = house;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getTown() {
		return town;
	}
	public void setTown(String town) {
		this.town = town;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	
	

}
