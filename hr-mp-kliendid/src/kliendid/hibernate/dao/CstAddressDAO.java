package kliendid.hibernate.dao;

import java.math.BigDecimal;

import kliendid.hibernate.model.CstAddress;

public interface CstAddressDAO extends GenericDAO<CstAddress, BigDecimal>{

}
