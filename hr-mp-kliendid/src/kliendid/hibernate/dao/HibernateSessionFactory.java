package kliendid.hibernate.dao;

/*
 * pmst sama, mis dbconnection.java, aga see mõeldud hibernate'i jaoks
 * sellega pannakse sessioon püsti
 */

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class HibernateSessionFactory {

	private static final Logger logger = Logger.getLogger(HibernateSessionFactory.class);
	
	private static final ThreadLocal<Session> threadLocal = new ThreadLocal<Session>();
	private static Configuration configuration = new Configuration();
	private static String confXml = "/hibernate.cfg.xml";
	private static SessionFactory sessionFactory = null;
	private static ServiceRegistry serviceRegistry;

	private HibernateSessionFactory() {
	}

	public static Session getSession() {

		Session session = (Session) threadLocal.get();

		if (session == null || !session.isOpen()) {
			if (sessionFactory == null) {
				createSessionFactory();
			}
			session = (sessionFactory != null) ? sessionFactory.openSession()
					: null;
			threadLocal.set(session);
		}

		return session;
	}

	/*public static void rebuildSessionFactory() {
		try {
			configuration.configure(confXml);
			sessionFactory = configuration.buildSessionFactory();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
	}*/
	
	public static void createSessionFactory() {

		configuration.configure(confXml);
	    serviceRegistry = new ServiceRegistryBuilder().applySettings(
	            configuration.getProperties()).build();
	    
	    sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	}

	public static void closeSession() {
		Session session = (Session) threadLocal.get();
		threadLocal.set(null);

		if (session != null) {
			session.close();
		}
	}

}
