package kliendid.hibernate.dao;

import java.math.BigDecimal;
import java.util.List;

import kliendid.hibernate.model.Employee;

public interface EmployeeDAO extends GenericDAO<Employee, BigDecimal>{

	public List<Employee> findEmployee(final String fistName);
}
