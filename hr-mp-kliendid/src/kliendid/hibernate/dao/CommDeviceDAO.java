package kliendid.hibernate.dao;

import java.math.BigDecimal;

import kliendid.hibernate.model.CommDevice;

public interface CommDeviceDAO extends GenericDAO<CommDevice, BigDecimal>{

}
