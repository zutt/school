package kliendid.hibernate.dao.impl;

import java.math.BigDecimal;

import kliendid.hibernate.dao.BaseHibernateDAO;
import kliendid.hibernate.dao.EmpUserDAO;
import kliendid.hibernate.model.EmpUser;

import org.apache.log4j.Logger;
import org.hibernate.Query;

public class EmpUserDAOImpl extends BaseHibernateDAO<EmpUser, BigDecimal> implements EmpUserDAO{

	private static final Logger logger = Logger.getLogger(EmpUserDAOImpl.class);

	public EmpUser getEmployeeByUserNameAndPassword(final String userName, final String password){
		
		String hql = "from EmpUser emp" + 
				" where emp.username = :username and emp.passw = :password";
		
		Query q = getSession().createQuery(hql);
		q.setString("username", userName);
		q.setString("password", password);
		
		if(q.uniqueResult() != null)
			return (EmpUser)q.uniqueResult();

		return null;
		
	}
}
