package kliendid.hibernate.dao.impl;

import java.math.BigDecimal;

import kliendid.hibernate.dao.BaseHibernateDAO;
import kliendid.hibernate.dao.CstAddressDAO;
import kliendid.hibernate.model.CstAddress;

import org.apache.log4j.Logger;

public class CstAddressDAOImpl extends BaseHibernateDAO<CstAddress, BigDecimal> implements CstAddressDAO{

	private static final Logger logger = Logger.getLogger(CstAddressDAOImpl.class);

}
