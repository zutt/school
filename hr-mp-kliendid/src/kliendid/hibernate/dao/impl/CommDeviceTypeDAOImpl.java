package kliendid.hibernate.dao.impl;

import java.math.BigDecimal;

import kliendid.hibernate.dao.BaseHibernateDAO;
import kliendid.hibernate.dao.CommDeviceTypeDAO;
import kliendid.hibernate.model.CommDeviceType;

import org.apache.log4j.Logger;

public class CommDeviceTypeDAOImpl extends BaseHibernateDAO<CommDeviceType, BigDecimal> implements CommDeviceTypeDAO{

	private static final Logger logger = Logger.getLogger(CommDeviceTypeDAOImpl.class);

}
