package kliendid.hibernate.dao.impl;

import java.math.BigDecimal;

import kliendid.hibernate.dao.BaseHibernateDAO;
import kliendid.hibernate.dao.CustomerGroupDAO;
import kliendid.hibernate.model.CustomerGroup;

import org.apache.log4j.Logger;

public class CustomerGroupDAOImpl extends BaseHibernateDAO<CustomerGroup, BigDecimal> implements CustomerGroupDAO{

	private static final Logger logger = Logger.getLogger(CustomerGroupDAOImpl.class);

}
