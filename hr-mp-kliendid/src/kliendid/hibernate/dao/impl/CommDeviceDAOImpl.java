package kliendid.hibernate.dao.impl;

import java.math.BigDecimal;

import kliendid.hibernate.dao.BaseHibernateDAO;
import kliendid.hibernate.dao.CommDeviceDAO;
import kliendid.hibernate.model.CommDevice;

import org.apache.log4j.Logger;

public class CommDeviceDAOImpl extends BaseHibernateDAO<CommDevice, BigDecimal> implements CommDeviceDAO{

	private static final Logger logger = Logger.getLogger(CommDeviceDAOImpl.class);

}
