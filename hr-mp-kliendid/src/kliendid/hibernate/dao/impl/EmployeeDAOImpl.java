package kliendid.hibernate.dao.impl;

import java.math.BigDecimal;
import java.util.List;

import kliendid.hibernate.dao.BaseHibernateDAO;
import kliendid.hibernate.dao.EmployeeDAO;
import kliendid.hibernate.model.Employee;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;

public class EmployeeDAOImpl extends BaseHibernateDAO<Employee, BigDecimal> implements EmployeeDAO{

	private static final Logger logger = Logger.getLogger(EmployeeDAOImpl.class);

	public List<Employee> findEmployee(final String firstName) {
		
		logger.info("Employee otsing");
		
		try {
			
			Criteria tingimused = getSession().createCriteria(Employee.class);

			if ((firstName != null)&&(firstName.length()>0)) {
				tingimused.add(Expression.like("firstName", firstName + "%").ignoreCase());
			}

			return tingimused.list();

		} catch (RuntimeException re) {
			logger.error("Otsing ei õnnestunud", re);
			throw re;
		}
	}
	
	public Employee getEmployeeByUserNameAndPassword(final String userName, final String password){
		
		return null;
	}
}
