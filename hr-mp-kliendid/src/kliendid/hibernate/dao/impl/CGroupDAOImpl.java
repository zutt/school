package kliendid.hibernate.dao.impl;

import java.math.BigDecimal;

import kliendid.hibernate.dao.BaseHibernateDAO;
import kliendid.hibernate.dao.CGroupDAO;
import kliendid.hibernate.model.CGroup;

import org.apache.log4j.Logger;

public class CGroupDAOImpl extends BaseHibernateDAO<CGroup, BigDecimal> implements CGroupDAO{

	private static final Logger logger = Logger.getLogger(CGroupDAOImpl.class);

}
