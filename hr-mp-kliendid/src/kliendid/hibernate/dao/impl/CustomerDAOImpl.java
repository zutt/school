package kliendid.hibernate.dao.impl;

import java.math.BigDecimal;
import java.util.List;

import kliendid.hibernate.dao.BaseHibernateDAO;
import kliendid.hibernate.dao.CustomerDAO;
import kliendid.hibernate.model.CstAddress;
import kliendid.hibernate.model.Customer;
import kliendid.model.SearchFilter;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Expression;

public class CustomerDAOImpl extends BaseHibernateDAO<Customer, BigDecimal>
		implements CustomerDAO {

	private static final Logger logger = Logger
			.getLogger(CustomerDAOImpl.class);

	public List<Customer> findCustomers(final SearchFilter filter) {

		logger.info("Customer otsing");

		try {

			Criteria tingimused = getSession().createCriteria(Customer.class);

			if (filter != null) {

				if ((filter.getFirstName() != null)
						&& (filter.getFirstName().length() > 0)) {
					tingimused.add(Expression.like("firstName",
							filter.getFirstName() + "%").ignoreCase());
				}

				if ((filter.getLastName() != null)
						&& (filter.getLastName().length() > 0)) {
					tingimused.add(Expression.like("lastName",
							filter.getLastName() + "%").ignoreCase());
				}
			}

			return tingimused.list();

		} catch (RuntimeException re) {
			logger.error("Otsing ei nnestunud", re);
			throw re;
		}
	}

	/*
	 * Allolevat meetodit me tegelikult ei kasutagi kuskil, see on teisiti realiseeritud
	 * (non-Javadoc)
	 * @see kliendid.hibernate.dao.CustomerDAO#getCustomerPrimaryAddress(java.math.BigDecimal)
	 */
	
	public CstAddress getCustomerPrimaryAddress(final BigDecimal customerId) {

		String hql = "from CstAddress address "
				+ "where address.customer = :customerId and address.addressType = 1";

		Query q = getSession().createQuery(hql);
		q.setBigDecimal("customerId", customerId);

		if(q.uniqueResult() != null)
			return (CstAddress)q.uniqueResult();
		
		return null;
	}

}
