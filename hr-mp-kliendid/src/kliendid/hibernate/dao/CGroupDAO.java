package kliendid.hibernate.dao;

import java.math.BigDecimal;

import kliendid.hibernate.model.CGroup;

public interface CGroupDAO extends GenericDAO<CGroup, BigDecimal>{

}
