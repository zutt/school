package kliendid.hibernate.dao;

import java.math.BigDecimal;
import java.util.List;

import kliendid.hibernate.model.CstAddress;
import kliendid.hibernate.model.Customer;
import kliendid.model.SearchFilter;

public interface CustomerDAO extends GenericDAO<Customer, BigDecimal>{

	public List<Customer> findCustomers(final SearchFilter filter);
	
	public CstAddress getCustomerPrimaryAddress(final BigDecimal customerId);
}
