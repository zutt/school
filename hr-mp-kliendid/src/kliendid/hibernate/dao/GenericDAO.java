package kliendid.hibernate.dao;

/*
 * kirjeldab meetodeid, mida H_DAO'd sisaldavad
 */

import java.io.Serializable;

import org.hibernate.Session;

public interface GenericDAO<T, ID extends Serializable> {

	public Session getSession();

	T saveOrUpdateEntity(T transientInstance);

	public void deleteEntity(final T entity);

	public T findById(ID id);
}
