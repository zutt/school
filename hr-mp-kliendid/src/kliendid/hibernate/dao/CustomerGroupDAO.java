package kliendid.hibernate.dao;

import java.math.BigDecimal;

import kliendid.hibernate.model.CustomerGroup;

public interface CustomerGroupDAO extends GenericDAO<CustomerGroup, BigDecimal>{

}
