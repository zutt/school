package kliendid.hibernate.dao;

import java.math.BigDecimal;

import kliendid.hibernate.model.EmpUser;

public interface EmpUserDAO extends GenericDAO<EmpUser, BigDecimal>{

	public EmpUser getEmployeeByUserNameAndPassword(final String userName, final String password);
}
