package kliendid.hibernate.dao;

/*
 * interface'i GenericDAO meetodite realiseerimine
 */

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class BaseHibernateDAO<T, ID extends Serializable> implements GenericDAO<T, ID> {

	private static final Logger logger = Logger.getLogger(BaseHibernateDAO.class);
	
	private Class<T> persistentClass;
	
	public Session getSession() {

		return HibernateSessionFactory.getSession();
	}
	
	public T saveOrUpdateEntity(T entity){
		try {

			Transaction tx = getSession().beginTransaction();

			getSession().saveOrUpdate(entity);
			tx.commit();

		} catch (RuntimeException re) {
			logger.error("Salvestamine eba�nnestus", re);
			throw re;
		}
		
		return entity;
	}
	
	public void deleteEntity(final T entity){
		
		try {

			Transaction tx = getSession().beginTransaction();

			getSession().delete(entity);
			tx.commit();
		} catch (RuntimeException re) {
			logger.error("Kustutamine eba�nnestus", re);
			throw re;
		}
	}
	
	public T findById(ID id){
		
		try {
			return (T) getSession().get(
					getPersistentClass(), id);

		} catch (RuntimeException re) {
			logger.error("Leidmine eba�nnestus", re);
			throw re;
		}
	}
	
	protected Class<T> getPersistentClass() {
		if (this.persistentClass == null) {
			return this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		} else
			return this.persistentClass;
	}
}
