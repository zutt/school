package kliendid.hibernate.dao;

import java.math.BigDecimal;

import kliendid.hibernate.model.CommDeviceType;

public interface CommDeviceTypeDAO extends GenericDAO<CommDeviceType, BigDecimal>{

}
