package kliendid.test;

import java.math.BigDecimal;
import java.util.List;

import junit.framework.TestCase;
import kliendid.hibernate.dao.CustomerDAO;
import kliendid.hibernate.dao.EmployeeDAO;
import kliendid.hibernate.dao.impl.CustomerDAOImpl;
import kliendid.hibernate.dao.impl.EmployeeDAOImpl;
import kliendid.hibernate.model.CstAddress;
import kliendid.hibernate.model.Employee;

public class HibernateTest extends TestCase{
	
	public void testFindById(){
		
		EmployeeDAO empDao = new EmployeeDAOImpl();
		
		Employee emp = empDao.findById(new BigDecimal(2));
		
		assertEquals("Jaani", emp.getFirstName());
	}
	
    public void testFindEmployee(){
		
		EmployeeDAO empDao = new EmployeeDAOImpl();
		
		List<Employee> empList = empDao.findEmployee("Jaani");
		
		assertEquals(1, empList.size());
	}
    
    /*public void testUpdateRecord(){
    	
    	EmployeeDAO empDao = new EmployeeDAOImpl();
		
		Employee emp = empDao.findById(new BigDecimal(6));

		emp.setLastName("hellekas1");
		
		empDao.saveOrUpdateEntity(emp);
    }*/
    
    public void testGetPrimaryAddress(){
    	
    	CustomerDAO custDao = new CustomerDAOImpl();
    	
    	CstAddress address =  custDao.getCustomerPrimaryAddress(new BigDecimal(2));
    	
    	assertEquals("Metsa", address.getAddress());
    }

}
