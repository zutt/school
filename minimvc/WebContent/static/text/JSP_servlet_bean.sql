CREATE SEQUENCE archive_doc_id ;

CREATE TABLE archive_doc 
( archive_doc_db_id integer NOT NULL DEFAULT nextval('archive_doc_id'),
  doc_nr varchar(10),
  name text,
  content text,
  price numeric(10,2),
  owner_id integer,
  doc_type integer,
  created date,
  updated date,
  CONSTRAINT my_document_pk PRIMARY KEY (archive_doc_db_id)
) ;


CREATE TABLE doc_type 
( doc_type integer ,
  name text,
  CONSTRAINT doc_type_pk PRIMARY KEY (doc_type)
) ;

CREATE SEQUENCE doc_owner_id ;

CREATE TABLE doc_owner
( doc_owner integer NOT NULL DEFAULT nextval('doc_owner_id'),
  identity_code varchar(20),
  name varchar(100),
  CONSTRAINT doc_owner_pk PRIMARY KEY (doc_owner)
) ;

INSERT INTO doc_type (doc_type, name) VALUES (1,'kasutajajuhend');
INSERT INTO doc_type (doc_type, name) VALUES (2,'juriidiline dokument');
INSERT INTO doc_type (doc_type, name) VALUES (3,'teadusartikkel/uurimus');

INSERT INTO doc_owner (name) VALUES ('Elektrikaupade Epood');
INSERT INTO doc_owner (name) VALUES ('Murubu teaduste Akadeemia');


INSERT INTO archive_doc (doc_nr,name, content,price,owner_id,created,updated) VALUES ('KJ34522220','Electrolux 56 juhend','Juhend Electrolux 56 kohta. Pohjalik',11.3,1, now(),now());
INSERT INTO archive_doc (doc_nr,name, content,price,owner_id,created,updated) VALUES ('KJ34522225','Samusng SM88 juhend','Juhend Samsung SM88 kohta. Hea',20.3,1, now(),now());
INSERT INTO archive_doc (doc_nr,name, content,price,owner_id,created,updated) VALUES ('TD00000001','Astroloogia teaduslikud aspektid','Artikkel astroloogiast.',200.3,2, now(),now());


