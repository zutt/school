<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<%@ page import="beer.model.Beer" %>
<jsp:useBean id="vastus" scope="request" type="beer.model.BeerForm" />
<jsp:useBean id="errors" scope="request" class="java.util.HashMap" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<link rel="stylesheet" type="text/css" href="static/css/view.css" /> 
<script type="text/javascript" src="static/js/beer.js"></script>
<title>BeerEditor</title>
</head>
<body>

	
	<img id="top" src="static/picture/top.png" alt="">
	<div id="form_container">
	

		<form id="form_814877" class="appnitro"   action="s?action=save" method="POST">
					<div class="form_description">
			<h2>Edit Beers
						<% if (errors.size() > 0) { 
					out.println("<span style='color:red;'>-    Data not saved due to errors</span>");
				}				
			%>
			
			</h2>
			<p><a href="/minimvc/s">Back to Servlet</a> | <a href='/minimvc/log/beerlog.txt'>log.txt</a> </p>
		</div>						
			<ul >
			
					<li id="li_1" >
		<label class="description" for="id">ID </label>
		<div>
			<input id="beer_id2" name="beer_id2" class="element text medium" type="text" maxlength="255" value="<%=vastus.getId() %>" disabled/>
			<input type="hidden" name="id" value="<%=vastus.getId() %>" />
		</div> 
		</li>		<li id="li_2" >
		<label class="description" for="beer_name">Name </label>
		<div>
			<input id="beer_name" name="beer_name" class="element text medium" type="text" maxlength="255" value="<%=vastus.getName() %>"/> 
			<% if (errors.get("name") != null) { 
					out.println("<span style='color:red;'>" + errors.get("name") + "</span>");
				}				
			%>


		</div> 
		</li>		<li id="li_3" >
		<label class="description" for="beer_type">Type </label>
		<div>
			<input id="beer_type" name="beer_type" class="element text medium" type="text" maxlength="255" value="<%=vastus.getType() %>"/>
						<% if (errors.get("type") != null) { 
					out.println("<span style='color:red;'>" + errors.get("type") + "</span>");
				}				
			%> 
		</div> 
		</li>	<li id="li_4" >
		<label class="description" for="beer_vol">Kangus </label>
		<div>
			<input id="beer_vol" name="beer_vol" class="element text medium" type="text" maxlength="255" value="<%=vastus.getVol() %>"/> 
						<% if (errors.get("vol") != null) { 
					out.println("<span style='color:red;'>" + errors.get("vol") + "</span>");
				}				
			%>
		</div> 
		</li>
			
					<li class="buttons">
			    <input type="hidden" name="form_id" value="814877" />
			    
				<input id="saveForm" class="button_text"  type="submit" name="submit" value="Save changes"/>
				<!-- onClick="post_data_to_server()" -->
				
		</li>
			</ul>
		</form>	

	</div>
	<img id="bottom" src="static/picture/bottom.png" alt="">

</body>
</html>