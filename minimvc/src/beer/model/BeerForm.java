package beer.model;

public class BeerForm {

	private String id ; 
	private String name;
	private String type; 
	private String vol ;
	
	public void setId(String id) {
		this.id = id; 
	}
	
	public void setName (String name) {
		this.name = name;
	}
	
	public void setType (String type) {
		this.type = type;
	}

	public void setVol(String vol) {
		this.vol = vol;
	}

	public String getId() {
		return this.id ;
	}

	public String getName () {
		return this.name ;
	}

	public String getType () {
		return this.type ;
	}

	public String getVol () {
		return this.vol ;
	}

	@Override
	public String toString() {
		return "Beer [id=" + id + ", name=" + name + ", type=" + type
				+ ", vol=" + vol + "]";
	}
	
}
