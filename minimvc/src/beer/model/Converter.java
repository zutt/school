package beer.model;

public class Converter {
	
	public static BeerForm convertToBeerForm(Beer beer){
		
		BeerForm beerForm = new BeerForm();
		
		beerForm.setId(String.valueOf(beer.getId()));
		beerForm.setName(String.valueOf(beer.getName()));
		beerForm.setType(String.valueOf(beer.getType()));
		beerForm.setVol(String.valueOf(beer.getVol()));
		
		return beerForm;
	}
	
	public static Beer convertToBeer(BeerForm beerForm){
		
		Beer beer = new Beer();
		
		beer.setId(Integer.parseInt(String.valueOf(beerForm.getId())));
		beer.setName(String.valueOf(beerForm.getName()));
		beer.setType(String.valueOf(beerForm.getType()));
		beer.setVol(Integer.parseInt(String.valueOf(beerForm.getVol())));
		
		return beer;
	}

}
