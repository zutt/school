package beer.model;

public class Beer {

	private int id ; 
	private String name;
	private String type; 
	private int vol ;
	
	public void setId(int id) {
		this.id = id; 
	}
	
	public void setName (String name) {
		this.name = name;
	}
	
	public void setType (String type) {
		this.type = type;
	}

	public void setVol(int vol) {
		this.vol = vol;
	}

	public int getId() {
		return this.id ;
	}

	public String getName () {
		return this.name ;
	}

	public String getType () {
		return this.type ;
	}

	public int getVol () {
		return this.vol ;
	}

	@Override
	public String toString() {
		return "Beer [id=" + id + ", name=" + name + ", type=" + type
				+ ", vol=" + vol + "]";
	}
	
}
