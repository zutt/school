package beer.controller;


import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import java.io.*;
import java.text.*;
import java.util.*;

import org.apache.log4j.Logger;

import beer.db.BeerDAO; 
import beer.model.Beer;
import beer.model.BeerForm;
import beer.model.Converter;

/**
 * Servlet implementation class Hello
 */
@WebServlet("/s")
public class BeerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	static Logger logger = Logger.getLogger(BeerServlet.class.getName());

	public  void init() {
		logger.info("BeerServlet.init(): Started up");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
		logger.info("Got a GET request: " + req);
		Locale locale = new Locale("et", "ESTONIA");
        Locale[] supportedLocales = {locale , Locale.ENGLISH};
        Locale currentLocale = locale;

		ResourceBundle labels = ResourceBundle.getBundle("LabelsBundle", currentLocale);
        String ButtonText = labels.getString("LABEL_RUNDOCQUERY_BUTTON");
	
        
		if (req.getParameter("id") != null) {
			//System.out.println("check1");
			if (!req.getParameter("id").matches("[0-9]+")) {
				//System.out.println("check2 - OK");
				req.setAttribute("vastus", req.getParameter("id"));
				req.getRequestDispatcher("error.jsp").forward(req, res);
			} else {
				//System.out.println("check3");
				Beer beer = BeerDAO.getBeersByID(req.getParameter("id"));
				if (beer == null) {
					//System.out.println("check4");
					req.setAttribute("vastus", req.getParameter("id"));
					req.getRequestDispatcher("error.jsp").forward(req, res);
				} else {
					//System.out.println("check5 " + beer);
					req.setAttribute("vastus", Converter.convertToBeerForm(beer));
					req.getRequestDispatcher("editbeers.jsp").forward(req, res);
				}
			}
		} else {
			//System.out.println("check6 - OK");
			req.setAttribute("nimekiri",BeerDAO.getAllBeersFromDB());		
			req.getRequestDispatcher("beerform.jsp").forward(req, res);
		}

	}




	public String getServletInfo() {
		return "DocServlet";
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
	throws IOException, ServletException {
		logger.info("Got a POST request: " + request);
		
		BeerForm beerForm = new BeerForm();
		beerForm.setId(request.getParameter("id"));
		beerForm.setName(request.getParameter("beer_name"));
		beerForm.setType(request.getParameter("beer_type"));
		beerForm.setVol(request.getParameter("beer_vol"));

		Beer okbeer = BeerDAO.getBeersByID(request.getParameter("id"));
		HashMap<String, String> errors = BeerValidator.validate(beerForm);
		if (errors.isEmpty()) {
			logger.info("Dataset did not contain errors");
			okbeer = Converter.convertToBeer(beerForm);
			//okbeer.setName(beerForm.getName());
			//okbeer.setType(beerForm.getType());
			//okbeer.setVol(Integer.parseInt(beerForm.getVol()));
			BeerDAO.updateBeer(okbeer);
		} else {
			logger.info("Dataset containes errors");
		}
		request.setAttribute("vastus", beerForm);
		request.setAttribute("errors", errors);
		logger.info("New data queried for displaying");
		request.getRequestDispatcher("editbeers.jsp").forward(request, response);
		logger.info("Displaying the dataset");
		
		//doGet(request, response);
	}
	public static void logThis(String log) {
		logger.info(log);
	}
}