package beer.controller;

import java.util.HashMap;

import beer.model.BeerForm;

public class BeerValidator {



		public BeerValidator(BeerForm beerForm) {
		// TODO Auto-generated constructor stub
	}

		public static HashMap<String, String> validate(BeerForm o){
			HashMap<String, String> errors = new HashMap<>();
			//System.out.println("valid1");
			if(o.getName() == null || o.getName() .trim().length() <=0)
				errors.put("name", "Field 'Name' has to be filled!");
			else if(o.getName() .trim().length() > 20)
				errors.put("name", "Field 'Name' max length 20 symbols!");
			
			if(o.getType()  == null || o.getType().trim().length() <=0)
				errors.put("type", "Field 'Type' has to be filled!");
			else if(o.getType().trim().length() > 20)
				errors.put("type", "Field 'Type' max length 20 symbols!");
			
			
			try{
				Integer.parseInt(o.getVol()) ;
			} catch(Exception e) {
				errors.put("vol", "Field 'Vol' has to be a number!");
			}
			  
			if (errors.get("vol").isEmpty()) {
				// TODO Tee kontoll 1-100
				if (o.getVol().length() > 2) 
				
					errors.put("vol", "Field 'Vol' has to be a number under 100!");
			}
					
		return errors;
	}
}
