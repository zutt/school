package beer.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beer.db.BeerDAO;
import beer.model.Beer;

/**
 * Servlet implementation class EditBeers
 */
@WebServlet("/EditBeers")
public class EditBeers extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditBeers() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String id = req.getParameter("id");
		//System.out.println("ÕLLE ID " + id);
		Beer beer = BeerDAO.getBeersByID(id);
		req.setAttribute("vastus",beer);
		req.getRequestDispatcher("WEB-INF/EditBeers.jsp").forward(req, res);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		//System.out.println("EDIT BEER!");
		Beer beer_to_post_to_db = new Beer();
		String current_beer_id_str = req.getParameter("id");
		int current_beer_id = Integer.valueOf(current_beer_id_str);
		beer_to_post_to_db.setId(current_beer_id);
		beer_to_post_to_db.setName(req.getParameter("beer_name"));
		beer_to_post_to_db.setType(req.getParameter("beer_type"));
		beer_to_post_to_db.setVol(Integer.valueOf(req.getParameter("beer_vol")));
		BeerDAO.updateBeer(beer_to_post_to_db);
		
		Beer beer = BeerDAO.getBeersByID(current_beer_id_str);
		req.setAttribute("vastus",beer);
		req.getRequestDispatcher("WEB-INF/EditBeers.jsp").forward(req, res);
		
	}

}
