package beer.db ;
import java.sql.* ;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import beer.controller.BeerServlet;
import beer.model.Beer;



public class BeerDAO {

	public static Beer getBeersByID(String beer_nr) {
		 String sql = "" ;
		 java.sql.Connection myConnection = null ;
		 ResultSet BeerHulk = null;
		 Statement st = null;
	  try {
	       myConnection = dbconnection.getConnection();
	       st = myConnection.createStatement();
		   sql = "select beer, name, type, vol from t111053_beer where beer = '" + beer_nr + "'" ;
		   BeerServlet.logThis("getBeersByID executed: " + sql);
		   BeerHulk = st.executeQuery(sql);
		   
		 
		   BeerHulk.next();
		   Beer beer_from_db = new Beer();
	   		beer_from_db.setId(BeerHulk.getInt("beer"));
	   		beer_from_db.setName(BeerHulk.getString("name"));
	   		beer_from_db.setType(BeerHulk.getString("type"));
	   		beer_from_db.setVol(BeerHulk.getInt("vol")); 
			myConnection.close();
			return beer_from_db;
	  }
			 
	   
	 catch(Exception ex)
	    {
	     BeerServlet.logThis("Error BeerDAO getBeersByID :" + ex.getMessage() + " Beer_Id=" + beer_nr );

	 }
	 		finally
			{
				dbconnection.closeStatement(st);
				dbconnection.closeResultSet(BeerHulk);
				dbconnection.close(myConnection);
			}
	 

	return null ;
	}
	
	public static List<Beer> getAllBeersFromDB() {
		 String sql = "" ;
		 java.sql.Connection myConnection = null ;
		 ResultSet BeerHulk = null;
		 Statement st = null;
	  try {
	       myConnection = dbconnection.getConnection();
	       st = myConnection.createStatement();
		   sql = "select beer, name, type, vol from t111053_beer" ;
		   BeerServlet.logThis("getAllBeersFromDB executed: " + sql);
		   BeerHulk = st.executeQuery(sql);
		   
		   List<Beer> vastus = new ArrayList<>();
		 
		   while(BeerHulk.next()) {
			   Beer beer_from_db = new Beer();
			   		beer_from_db.setId(BeerHulk.getInt("beer"));
			   		beer_from_db.setName(BeerHulk.getString("name"));
			   		beer_from_db.setType(BeerHulk.getString("type"));
			   		beer_from_db.setVol(BeerHulk.getInt("vol")); 
			   vastus.add(beer_from_db);
							  }
	         myConnection.close();
	         return vastus;
			 
	  }
	   
	 catch(Exception ex)
	    {
		 BeerServlet.logThis("Error BeerDAO getAllBeersFromDB" + ex.getMessage() );

	 }
	 		finally
			{
				dbconnection.closeStatement(st);
				dbconnection.closeResultSet(BeerHulk);
				dbconnection.close(myConnection);
			}
	return null;
	
	}
	
	public static void updateBeer(Beer beer) {
		 String sql = "" ;
		 java.sql.Connection myConnection = null ;
		 ResultSet BeerHulk = null;
		 PreparedStatement st = null;
	  try {
	       myConnection = dbconnection.getConnection();
	       sql = "update t111053_beer set type = ?, vol = ?, name = ? where beer=?" ;
	       st = myConnection.prepareStatement(sql);
	       st.setString(1, beer.getType());
	       st.setInt(2, beer.getVol());
	       st.setString(3, beer.getName());
	       st.setInt(4, beer.getId());
		   st.executeUpdate();
	       BeerServlet.logThis("updateBeer executed: " + sql);
	       myConnection.close();
			 
	  }
	   
	 catch(Exception ex)
	    {
		 BeerServlet.logThis("Error BeerDAO updateBeer" + ex.getMessage()  );

	 }
	 		finally
			{
				dbconnection.closeStatement(st);
				dbconnection.closeResultSet(BeerHulk);
				dbconnection.close(myConnection);
			}
	 
	}


}
