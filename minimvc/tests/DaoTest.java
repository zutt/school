

import static org.junit.Assert.*;

import org.junit.Test;

import beer.db.BeerDAO;
import beer.model.Beer;


public class DaoTest {

	@Test
	public void testResultSize() {
		assertEquals(3, BeerDAO.getAllBeersFromDB().size() );
	}
	@Test
	public void testBeerId() {
		assertEquals(1, BeerDAO.getBeersByID("1").getId() );
	}
	
	
	@Test
	public void updateBeerId() {
		Beer beer = new Beer();
		beer.setId(1);
		beer.setName("Sarvik");
		beer.setType("Bock");
		beer.setVol(5);
		BeerDAO.updateBeer(beer);
		assertEquals(5, BeerDAO.getBeersByID("1").getVol() );
		

	}


}
